﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using CNT;
using System.Threading;
using Heterodyne.Localization;
using System.IO.Ports;
using System.Globalization;
using System.Reflection;
using System.ComponentModel;
using Heterodyne.PropGridClasses;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region parametrs
        private int _minFreq = 1000;
        private int _maxFreq = 6000000;
        private int _minStepPprch = 25;
        private const int _maxStepPprch = 1000;
        private const int _minSaltusPprch = 2;
        private const int _maxSaltusPprch = 2000;
        private string _codeMessFromShaper = "";
        private string _temp = "";
        private string _humidid = "";
        private string _charge = "";
        public bool FlagOFF = false;
        private ComHTRD _comHTRD;
        private static ushort _timeSend = 1000;
        private static ushort _timeSend_FrequenceChanged = 1000;
        private static ushort _pollTimeDelay = 1000;
        private static ushort _timeDelay = 1000;
        private int _countMaxEnter;
        private byte[] _numberFreq = { 2, 4, 8, 16, 32, 64, 121 };
        private byte[] _arrayStepFreq = { 1, 2, 5, 15 };
        private int _currentFreq = 0;
        private int _maxCountReq = 5;
        private List<Language1> _list1;
        private Language1 _len;
        public static string path;
        private bool _stateSend { get; set; }
        private Boolean _flagFirstLaunch { get; set; }
        private int _koef;
        private Thread FWSworker;
        #endregion


        public MainWindow()
        {
            InitializeComponent();

            PrGr.OnVersionTypeChanged += PrGr_OnVersionTypeChanged;
        }

        private void PrGr_OnVersionTypeChanged(object sender, PropGridClasses.Version e)
        {
            if (e == PropGridClasses.Version.Main)
            { 
                FUSSCoefficient = 25;
                FrequencyCoefficient = 1;
                _minFreq = 1000;
                _maxFreq = 6000000;
                _minStepPprch = 25;
            }
            else
            {
                FUSSCoefficient = 5;
                FrequencyCoefficient = 100;
                _minFreq = 1500;
                _maxFreq = 30000;
                _minStepPprch = 5;
            }
        }

        private void Button_Click_Off(object sender, RoutedEventArgs e)
        {
            ShowChackedRadioButtonFalse();
            ShowStateGrid();
 
            this.Dispatcher.Invoke(() =>
            {
                if (_tmAutoEnter != null)
                    _tmAutoEnter.Dispose();
                if (_tmAutoEnterFWS != null)
                    _tmAutoEnterFWS.Dispose();
                if (_tmAutoEnterSwitchingFreq != null)
                    _tmAutoEnterSwitchingFreq.Dispose();
            });
            _countMaxEnter = 0;
           
            FlagOFF = true;

            ShowStateRadioButton(false);
            ShowStateButton(SwitchOff, false);
            ShowStateButton(Update, false);
            ShowStateButton(SwitchFrequenceButton, false);

            if (PrGr.MySettings.AutoFWS != true)
            {
                mrse = null;
                if (_tmAutoEnterFWS != null)
                    _tmAutoEnterFWS.Dispose();
                if (FWSworker != null)
                {
                    FWSworker.Abort();
                    FWSworker = null;                  
                }
            }
            Cont.Visibility = Visibility.Collapsed;
            if (_comHTRD != null)
                SpecialSend(0, ComHTRD.OffSignal, 0, 0, 0, 0);
            try
            {
                //ErrorTextBox.Text = _len.StatusBar2;
                _codeMessFromShaper = "";
            }
            catch {// ErrorTextBox.Text = "No error"; 
            }
                 _initializationFlag = false;
            _flagFirstLaunch = true;
        }        
     
        private void HTRD_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (_flagFirstLaunch == false)
                _flagFirstLaunch = true;
            string[] portnames = SerialPort.GetPortNames();
            int index = Array.IndexOf(portnames, PrGr.MySettings.ComPortCommon);
            if (_servConected == false)
            {
                InitConnectionHTRD();
            }
            else
            {
                if (_comHTRD != null)
                {
                    _comHTRD.ClosePort();
                    _servConected = false;
                    _timerInitialization.Start();
                    ShowStateRadioButton(false);
                    ShowStateButton(Update, false);
                    ShowStateButton(SwitchOff, false);
                    ShowStateButton(SwitchFrequenceButton, false);
                }
            }
        }

        private void MW_Initialized(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Properties.Settings.Default.Language);
               CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(Properties.Settings.Default.Language);
            }
            catch (Exception){  }

                _countMaxEnter = 0;
                _green = new RadialGradientBrush(Color.FromRgb(106, 232, 186), Color.FromRgb(15, 170, 121));
                _comHTRD = new ComHTRD();
                ShowStateRadioButton(false);
                ShowStateButton(Update, false);
                ShowStateButton(SwitchOff, false);
                ShowStateButton(SwitchFrequenceButton, false);
                ShowStateDataTextBox();
                _flagFirstLaunch = true;
                _stateSend = false;

                InitTimers();
                InitDelegate();
                InitBackgroundWorker();
                InitConnectionHTRD();
                InitLocalization();
                ColExpLog.IsChecked = true;
                ColExpPG.IsChecked = true;
                FUSSGrid.Visibility = Visibility.Collapsed;
                FWSGrid.Visibility = Visibility.Collapsed;
                FWGrid.Visibility = Visibility.Collapsed;
                PrGr.OnChange += InitLocalization;
                PrGr.Update += UpdateComSpeedAndComPort;
               
                Log.TimeColor = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                Log.StartLine = "App loaded.\n";
                Log.AddTextToLog("App loaded.\n", Brushes.LightGray, _koef);


            if (Convert.ToInt32(PG.ini.IniReadValue("Parameters", "frequency")) == 0)
            {
                frequence = FrequenceForSwitching._434;
                SwitchFrequenceButton.Content = _len.Switch2; 
            }
            if (Convert.ToInt32(PG.ini.IniReadValue("Parameters", "frequency")) == 1)
            {
                frequence = FrequenceForSwitching._868;
                SwitchFrequenceButton.Content = _len.Switch1; 
            }
        }

        private void Cont_Click(object sender, RoutedEventArgs e)
        {
            _comHTRD.Send(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
            PreConvertedFrequency -= PrGr.MySettings.StepFWS;
            Resume();          
            con++;
            Cont.Visibility = Visibility.Collapsed;
        }

        private FrequenceForSwitching frequence = FrequenceForSwitching._434;
        private void SwitchFrequenceButton_Click(object sender, RoutedEventArgs e)
        {
            if (_comHTRD != null)
            {
                if(frequence == FrequenceForSwitching._434)
                {
                    frequence = FrequenceForSwitching._868;
                    SwitchFrequenceButton.Content = _len.Switch1;
                    SpecialSendSwitchingCommand(frequence);
                    PG.ini.IniWriteValue("Parameters", "frequence",Convert.ToString(0));
                    return;
                }
                if (frequence == FrequenceForSwitching._868)
                {
                    frequence = FrequenceForSwitching._434;
                    SwitchFrequenceButton.Content = _len.Switch2;
                    SpecialSendSwitchingCommand(frequence);
                    PG.ini.IniWriteValue("Parameters", "frequence", Convert.ToString(1));
                    return;
                }
            }
        }

        private void MW_Closed(object sender, EventArgs e)
        {
            _comHTRD?.ClosePort();
            _tmWriteByteHTRD?.Dispose();
            _tmReadByteHTRD?.Dispose();
            _tmDurationRadiationHTRD?.Dispose();
            _tmAutoEnter?.Dispose();
            _tmAutoEnterFWS?.Dispose();
            _tmAutoEnterSwitchingFreq?.Dispose();
            _timerInitialization?.Stop();
            _timerOpen?.Stop();
            UpdateParameters?.Dispose();
            tr?.Abort();
        }
    }
}
       

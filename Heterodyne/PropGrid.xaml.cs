﻿using Heterodyne.PropGridClasses;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Input;
using Version = Heterodyne.PropGridClasses.Version;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для PropGrid.xaml
    /// </summary>
    public partial class PropGrid : UserControl
    {
        DictionaryForLocalizationEN Lan = new DictionaryForLocalizationEN(); 
        public PropGrid()
        {          
            InitializeComponent();
            MySettings.PropertyChanged += PG_PropertyChanged;
            InitEditors();
            InitializeLanguages();
            CollapsedCategories();
        }     
        private PG _oldSettings;

        public PG MySettings
        {
            get
            {
                var a = (PG)Resources["bb"];
                return a;
            }
            set
            {
                if ((PG)Resources["bb"] == value)
                {
                    _oldSettings = value.Clone();
                    return;
                }
                ((PG)Resources["bb"]).Update(value);
                _oldSettings = MySettings.Clone();
            }           
        }

        public delegate void LocalizationHandler();
        public event LocalizationHandler OnChange;

        public delegate void UpdateHandler();
        public event UpdateHandler Update;

        public event EventHandler<PropGridClasses.Version> OnVersionTypeChanged = (sender, isCorrect) => { };

        public void InitEditors()
        {
            propertyGrid.Editors.Add(new Editor(nameof(PG.SpeedCommon), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.ComPortCommon), MySettings.GetType()));
            propertyGrid.Editors.Add(new Editor(nameof(PG.Language), MySettings.GetType()));

            propertyGrid.Editors.Add(new Editor(nameof(PG.FrequencyFUSS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.AmountOfIterationFUSS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.AmountOfSignalPerSecFUSS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.StepOfFreqencyFUSS), MySettings.GetType())); 

            propertyGrid.Editors.Add(new Editor(nameof(PG.StepFWS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.AutoFWS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.TimeFWS), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.FrequencyFWS), MySettings.GetType())); 

            propertyGrid.Editors.Add(new Editor(nameof(PG.RangeFW), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.StepFW), MySettings.GetType())); 
            propertyGrid.Editors.Add(new Editor(nameof(PG.Offset), MySettings.GetType()));
            propertyGrid.Editors.Add(new Editor(nameof(PG.Duration), MySettings.GetType()));
            propertyGrid.Editors.Add(new Editor(nameof(PG.EndFreqencyFWS), MySettings.GetType()));   
            //propertyGrid.Editors.Add(new Editor(nameof(PG.VersionType), MySettings.GetType()));
        }

        public void InitProperties()
        {
            _oldSettings = MySettings.Clone();
            foreach (var property in propertyGrid.Properties)
            {
                property.PropertyValue.PropertyValueException += MyPropertyException;
            }
        }

        #region Properties exceptions
        private void MyPropertyException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion

        #region PropertyGrid key for buttons
        private void PropertyGrid_KeyUp(object sender, KeyEventArgs e)
        {       
            if (e.Key == Key.Add) //&& ((bool)propertyGrid.Editors[0].GetValue(DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(TextBox))))==true)
            {
                propertyGrid.Properties[1].SetValue((Double)propertyGrid.Properties[1].GetValue() +  1000);
               // propertyGrid.Properties[1].SetValue((Double)propertyGrid.Properties[1].GetValue() + (Int32)propertyGrid.Properties[4].GetValue());
            }
            if (e.Key == Key.Subtract) //&& ((bool)propertyGrid.Editors[0].GetValue(DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(TextBox))))==true)
            {
                if((Double)propertyGrid.Properties[1].GetValue() < 1000 * (Int32)propertyGrid.Properties[4].GetValue())
                    return;
                propertyGrid.Properties[1].SetValue((Double)propertyGrid.Properties[1].GetValue() - 1000);
                // propertyGrid.Properties[1].SetValue((Double)propertyGrid.Properties[1].GetValue() + (Int32)propertyGrid.Properties[4].GetValue());
            }

            if (e.Key == Key.Up)
            {
                propertyGrid.Properties[0].SetValue((Int32)propertyGrid.Properties[0].GetValue() + 1);//(int)propertyGrid.Properties[0].GetValue() + 1);
            }

            if (e.Key == Key.Down)
            {
                if ((Int32)propertyGrid.Properties[0].GetValue() <= 0)
                    return;
                propertyGrid.Properties[0].SetValue((Int32)propertyGrid.Properties[0].GetValue() - 1);//(int)propertyGrid.Properties[0].GetValue() + 1);
            }
        }
        #endregion

        #region localization function

        private void InitializeLanguages()
        {
            if (Properties.Settings.Default.Language == "ru-RU")
            {
                MySettings.Language = "RU";
                Init();
            }
            else if (Properties.Settings.Default.Language == "en-EN")
            {
                MySettings.Language = "EN";
                Init();
            }
        }
        private void PG_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {           
            if (e.PropertyName == "Language")
            {
                var listProperties = this.propertyGrid.Properties;

                if (MySettings.Language == "EN")
                {
                    Properties.Settings.Default.Language = "en-EN";
                    Properties.Settings.Default.Save();
                  
                    for (int i = 0; i < listProperties.Count; i++)
                    {
                        ChangeDescriptionAttribute(Lan.ENDiscrDict[listProperties[i].Name], listProperties[i].Name);
                        ChangeNameAttribute(Lan.ENPropDict[listProperties[i].Name], listProperties[i].Name);
                    }
                    Init();

                    foreach (var nameCategory in propertyGrid.Categories.Select(category => category.Name).ToList())
                    {
                       propertyGrid.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = Lan.ENCateg[nameCategory];
                    }

                    OnChange?.Invoke();
                }
                if (MySettings.Language == "RU")
                {
                    Properties.Settings.Default.Language = "ru-RU";
                    Properties.Settings.Default.Save();
                   
                   
                    for (int i = 0; i < listProperties.Count; i++)
                    {
                        ChangeDescriptionAttribute(Lan.RUDiscrDict[listProperties[i].Name], listProperties[i].Name);
                        ChangeNameAttribute(Lan.RUPropDict[listProperties[i].Name], listProperties[i].Name);
                    }

                    Init();
                    foreach (var nameCategory in propertyGrid.Categories.Select(category => category.Name).ToList())
                    {
                        propertyGrid.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = Lan.RUCateg[nameCategory];
                    }

                    OnChange?.Invoke();
                }

                CollapsedCategories();             
            }
            else if (e.PropertyName == "ComPortCommon" || e.PropertyName == "SpeedCommon")
            {
                Update?.Invoke();
            }
            else if (e.PropertyName == "VersionType")
            {
                OnVersionTypeChanged(sender, MySettings.VersionType);
            }
        }

        private void Init()
        {
            MySettings = propertyGrid.SelectedObject as PG;
            propertyGrid.SelectedObject = MySettings;
            InitEditors();
            var list = this.propertyGrid.Categories;
            list[0].HeaderCategoryName = "Общее";
        }

        private void ChangeDescriptionAttribute(string value, string property)
        {          
            PropertyDescriptor pDesc = TypeDescriptor.GetProperties(MySettings.GetType())[property];
            DescriptionAttribute attrib = (DescriptionAttribute)pDesc.Attributes[typeof(DescriptionAttribute)];
            FieldInfo isBrowsable = attrib.GetType().GetField("description", BindingFlags.NonPublic | BindingFlags.Instance);
            isBrowsable.SetValue(attrib, value);
            var item = propertyGrid.SelectedObject as PG;
            propertyGrid.SelectedObject = new PG()
            {
                SpeedCommon = item.SpeedCommon,
                AmountOfIterationFUSS = item.AmountOfIterationFUSS,
                AmountOfSignalPerSecFUSS = item.AmountOfSignalPerSecFUSS,
                AutoFWS = item.AutoFWS,
                ComPortCommon = item.ComPortCommon,
                Duration = item.Duration,
                EndFreqencyFWS = item.EndFreqencyFWS,
                FrequencyFUSS = item.FrequencyFUSS,
                FrequencyFWS = item.FrequencyFWS,
                Language = item.Language,
                Offset = item.Offset,
                RangeFW = item.RangeFW,
                StepFW = item.StepFW,
                StepFWS = item.StepFWS,
                StepOfFreqencyFUSS = item.StepOfFreqencyFUSS,
                TimeFWS = item.TimeFWS,
                VersionType = item.VersionType,
            };
        }

        private void ChangeNameAttribute(string value, string property)
        {
            PropertyDescriptor pDesc = TypeDescriptor.GetProperties(MySettings.GetType())[property];
            DisplayNameAttribute attrib = (DisplayNameAttribute)pDesc.Attributes[typeof(DisplayNameAttribute)];
            FieldInfo isBrowsable = attrib.GetType().GetField("_displayName", BindingFlags.NonPublic | BindingFlags.Instance);
            isBrowsable.SetValue(attrib, value);
            var item = propertyGrid.SelectedObject as PG;
            propertyGrid.SelectedObject = new PG()
            {
                SpeedCommon = item.SpeedCommon,
                AmountOfIterationFUSS = item.AmountOfIterationFUSS,
                AmountOfSignalPerSecFUSS = item.AmountOfSignalPerSecFUSS,
                AutoFWS = item.AutoFWS,
                ComPortCommon = item.ComPortCommon,
                Duration = item.Duration,
                EndFreqencyFWS = item.EndFreqencyFWS,
                FrequencyFUSS = item.FrequencyFUSS,
                FrequencyFWS = item.FrequencyFWS,
                Language = item.Language,
                Offset = item.Offset,
                RangeFW = item.RangeFW,
                StepFW = item.StepFW,
                StepFWS = item.StepFWS,
                StepOfFreqencyFUSS = item.StepOfFreqencyFUSS,
                TimeFWS = item.TimeFWS,
                VersionType = item.VersionType,
            };
        }


            #endregion

            public void CollapsedCategories()
        {
            var list = this.propertyGrid.Categories;
            list[1].IsExpanded = false;
            list[2].IsExpanded = false;
            list[3].IsExpanded = false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для Log_RichTextBox.xaml
    /// </summary>
    public partial class Log_RichTextBox : UserControl
    {
     
        public Log_RichTextBox()
        {          
            InitializeComponent();
           _myFlowDoc = new FlowDocument();
            _myParagraph = new Paragraph();
            _txtRange = new List<string>();
            _time = new List<string>();
            _brushesList = new List<Brush>();
        }

        public bool DisplayTime { get; set; } = true; //отображать ли время при выводе
        public string StartLine { get; set; } = ""; //текст выводящийся после отчищения поля
        public Brush StartLineColor { get; set; } = Brushes.LightGray; // цвет StartLine
        public Brush TimeColor { get; set; } = Brushes.Aquamarine; // цвет выводимого времени
        public int FontSizeKoefitient { get; set; } = 0; // коэффициент изменения шрифта текста (добавляется к основному)
        public bool ShowAllByte { get; private set; }

        private List<string> _txtRange;
        private List<string> _time;
        private List<Brush> _brushesList;
        private FlowDocument _myFlowDoc;
        private Paragraph _myParagraph;
        private int ValueForClear = 35;
        private int i = 0;


        public void AddTextToLog(string text, Brush LogBrush, double IncreaseFontSizeBy)
        {
            Run HelloMessage = new Run(" " + text + "\n");
            HelloMessage.FontFamily = new FontFamily("Times New Roman");
            HelloMessage.Foreground = LogBrush;
            HelloMessage.FontSize = richTextBox.FontSize + IncreaseFontSizeBy;

            if (DisplayTime == true)
            {
                Run HelloRun = new Run(DateTime.Now.ToLongTimeString().Trim());
                HelloRun.FontFamily = new FontFamily("Times New Roman");
                HelloRun.FontSize = richTextBox.FontSize + IncreaseFontSizeBy;
                HelloRun.Foreground = TimeColor;
                _myParagraph.Inlines.Add(HelloRun);
                _time.Add(HelloRun.Text);
            }

            _myParagraph.Inlines.Add(HelloMessage);
            _myFlowDoc.PageWidth = richTextBox.Width;
            _myFlowDoc.IsOptimalParagraphEnabled = true;
            _myFlowDoc.IsHyphenationEnabled = true;
            _myFlowDoc.Blocks.Add(_myParagraph);

            richTextBox.Document = _myFlowDoc;
            _txtRange.Add(HelloMessage.Text);
            _brushesList.Add(HelloMessage.Foreground);
            LogFontSize(IncreaseFontSizeBy);
            richTextBox.ScrollToEnd();

            if (i > ValueForClear)
            {
                Button_Click(new object(), new RoutedEventArgs());
            }

            i++;
        }

        public void AddTextToLog(string text, Brush LogBrush)
        {
            Run HelloMessage = new Run(" " + text + "\n");
            HelloMessage.FontFamily = new FontFamily("Times New Roman");
            HelloMessage.Foreground = LogBrush;
            HelloMessage.FontSize = richTextBox.FontSize;

            if (DisplayTime == true)
            {
                Run HelloRun = new Run(DateTime.Now.ToLongTimeString().Trim());
                HelloRun.FontFamily = new FontFamily("Times New Roman");
                HelloRun.FontSize = richTextBox.FontSize;
                HelloRun.Foreground = TimeColor;
                _myParagraph.Inlines.Add(HelloRun);
                _time.Add(HelloRun.Text);
            }

            _myParagraph.Inlines.Add(HelloMessage);
            _myFlowDoc.PageWidth = richTextBox.Width;
            _myFlowDoc.IsOptimalParagraphEnabled = true;
            _myFlowDoc.IsHyphenationEnabled = true;
            _myFlowDoc.Blocks.Add(_myParagraph);

            richTextBox.Document = _myFlowDoc;
            _txtRange.Add(HelloMessage.Text);
            _brushesList.Add(HelloMessage.Foreground);
            richTextBox.ScrollToEnd();

            if (i > ValueForClear)
            {
                Button_Click(new object(), new RoutedEventArgs());
            }

            i++;
        }

        public void AddTextToLog(string text)
        {
            Run HelloMessage = new Run(" " + text + "\n");
            HelloMessage.FontFamily = new FontFamily("Times New Roman");
            HelloMessage.FontSize = richTextBox.FontSize;

            if (DisplayTime == true)
            {
                Run HelloRun = new Run(DateTime.Now.ToLongTimeString().Trim());
                HelloRun.FontFamily = new FontFamily("Times New Roman");
                HelloRun.FontSize = richTextBox.FontSize;
                HelloRun.Foreground = TimeColor;
                _myParagraph.Inlines.Add(HelloRun);
                _time.Add(HelloRun.Text);
            }

            _myParagraph.Inlines.Add(HelloMessage);
            _myFlowDoc.PageWidth = richTextBox.Width;
            _myFlowDoc.IsOptimalParagraphEnabled = true;
            _myFlowDoc.IsHyphenationEnabled = true;
            _myFlowDoc.Blocks.Add(_myParagraph);

            richTextBox.Document = _myFlowDoc;
            _txtRange.Add(HelloMessage.Text);
            _brushesList.Add(HelloMessage.Foreground);
            richTextBox.ScrollToEnd();

            if (i > ValueForClear)
            {
                Button_Click(new object(), new RoutedEventArgs());
            }

            i++;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            richTextBox.Document.Blocks.Clear();
            _myFlowDoc.Blocks.Clear();
            _myParagraph.Inlines.Clear();
            _txtRange.Clear();
            _time.Clear();
            _brushesList.Clear();
            i = 0;

            if (StartLine != "")
                AddTextToLog(StartLine, StartLineColor, FontSizeKoefitient);
        }

        public void LogFontSize(double p)
        {
            FlowDocument myFlowDoc = new FlowDocument();
            Paragraph myParagraph = new Paragraph();

            for (int i = 0; i < _time.Count; i++)
            {
                Run myRun11 = new Run(_time[i].Trim());
                Run myRun12 = new Run(_txtRange[i]);
                myRun11.FontFamily = new FontFamily("Times New Roman");
                myRun11.FontSize = 12 + p;
                myRun11.Foreground = TimeColor;
                myRun12.FontFamily = new FontFamily("Times New Roman");
                myRun12.Foreground = _brushesList[i];
                myRun12.FontSize = 12 + p;

                myParagraph.Inlines.Add(myRun11);
                myParagraph.Inlines.Add(myRun12);
                myFlowDoc.PageWidth = richTextBox.Width;
                myFlowDoc.IsOptimalParagraphEnabled = true;
                myFlowDoc.IsHyphenationEnabled = true;
                myFlowDoc.Blocks.Add(myParagraph);
                richTextBox.Document = myFlowDoc;
            }
        }

        private void ShowAllByte_Checked(object sender, RoutedEventArgs e) => ShowAllByte = true;

        private void AllByte_Unchecked(object sender, RoutedEventArgs e) => ShowAllByte = false;

    }
}

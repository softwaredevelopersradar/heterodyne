﻿using Heterodyne.PropGridInfClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.WpfPropertyGrid;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для PropertyGridInfo.xaml
    /// </summary>
    public partial class PropertyGridInfo : UserControl
    {
        public PropertyGridInfo()
        {
            InitializeComponent();
        }

        private PGInf oldSettings;

        public PGInf MySettings
        {
            get
            {
                var a = (PGInf)Resources["bb"];
                return a;
            }
            set
            {
                if ((PGInf)Resources["bb"] == value)
                {
                    oldSettings = value.Clone();
                    return;
                }
                ((PGInf)Resources["bb"]).Update(value);
                oldSettings = MySettings.Clone();
            }
        }


        public void InitEditors()
        {

            propertyGridInfo.Editors.Add(new Editor1(nameof(PGInf.LenguageCommon), MySettings.GetType())); //0
            propertyGridInfo.Editors.Add(new Editor1(nameof(PGInf.SpeedCommon), MySettings.GetType())); //1
            propertyGridInfo.Editors.Add(new Editor1(nameof(PGInf.ComPortCommon), MySettings.GetType())); //2
        }

        private void InitProperties()
        {
            oldSettings = MySettings.Clone();
            foreach (var property in propertyGridInfo.Properties)
            {
                property.PropertyValue.PropertyValueException += MyPropertyException;
            }
        }

        #region Properties exceptions
        private void MyPropertyException(object sender, ValueExceptionEventArgs e)
        {
            propertyGridInfo.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion

    }
}

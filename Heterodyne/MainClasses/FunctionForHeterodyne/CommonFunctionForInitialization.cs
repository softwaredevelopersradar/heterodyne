﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private void ShowStateButton(Button button, bool state)
        {
            if (!button.Dispatcher.CheckAccess())
            {
                button.Dispatcher.Invoke(() =>
                {
                    button.IsEnabled = state;
                });
            }
            else
            {
                button.IsEnabled = state;
            }
        }

        private void ShowColorReadWrite(RadialGradientBrush color)
        {
            if (!HTRD.Dispatcher.CheckAccess())
            {
                HTRD.Dispatcher.Invoke(() =>
                {
                    HTRD.WriteDataColor = color;
                });
            }
            else
            {
                HTRD.WriteDataColor = color;
            }
        }

        private void ShowColorButton(Brush color)
        {
            if (!HTRD.Dispatcher.CheckAccess())
            {
                HTRD.Dispatcher.Invoke(() =>
                {
                    HTRD.ButServerColor = color;
                });
            }
            else
            {
                HTRD.ButServerColor = color;
            }
        }

        private void ShowStateOneRadioButton(RadioButton OneRB, bool state)
        {
            if (!OneRB.Dispatcher.CheckAccess())
            {
                OneRB.Dispatcher.Invoke(() =>
                {
                    OneRB.IsEnabled = true;
                });
            }
            else
            {
                OneRB.IsEnabled = true;
            }
        }

        private void ShowStateRadioButton(bool state)
        {
            ShowStateOneRadioButton(CheckFUSS, state);
            ShowStateOneRadioButton(CheckFWS, state);
            ShowStateOneRadioButton(CheckFW, state);
        }

        private void ShowInfoLabel(TextBlock TB, string strInfo)
        {
            if (!TB.Dispatcher.CheckAccess())
            {
                TB.Dispatcher.Invoke(() =>
                {
                    TB.Text = strInfo;
                });
            }
            else
            {
                TB.Text = strInfo;
            }
        }

        private void ShowStateLabel(TextBlock TB, Brush color)
        {
            if (!TB.Dispatcher.CheckAccess())
            {
                TB.Dispatcher.Invoke(() =>
                {
                    TB.Foreground = color;
                });
            }
            else
            {
               TB.Foreground = color;
            }
        }

        private void ShowStateOneGrid(Grid OneGrid)
        {
            if (!OneGrid.Dispatcher.CheckAccess())
            {
                OneGrid.Dispatcher.Invoke(() =>
                {
                    OneGrid.Visibility = Visibility.Collapsed;
                });
            }
            else
            {
                OneGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowStateGrid()
        {
          ShowStateOneGrid(FUSSGrid);
          ShowStateOneGrid(FWSGrid);
          ShowStateOneGrid(FWGrid);
        }

        private void ShowStatePropertyGrid(bool state)
        {
            if (!PrGr.Dispatcher.CheckAccess())
            {
                PrGr.Dispatcher.Invoke(() =>
                {
                   PrGr.IsEnabled = state;
                });
            }
            else
            {
                PrGr.IsEnabled = state;
            }
        }
         private void ShowChackedRadioButtonFalse()
        {
            if (!Dispatcher.CheckAccess())
            {
               Dispatcher.Invoke(() =>
                {
                    CheckFUSS.IsChecked = false;
                    CheckFWS.IsChecked = false;
                    CheckFW.IsChecked = false;
                });
            }
            else
            {
                CheckFUSS.IsChecked = false;
                CheckFWS.IsChecked = false;
                CheckFW.IsChecked = false;
            }
        }

        private void ShowStateDataTextBox()
        {
            if (!GridData.Dispatcher.CheckAccess())
            {
                GridData.Dispatcher.Invoke(() =>
                {
                    errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                    errorTB.Text = "  -";
                    powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                    powerTB.Text = "  -";
                    temeratureTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                    temeratureTB.Text = "  0 °C";
                    humidityTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                    humidityTB.Text = "  0 %";
                    chargeTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                    chargeTB.Text = "   0 V";
                });
            }
            else
            {
                errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                errorTB.Text = "  -";
                powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                powerTB.Text = "  -";
                temeratureTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                temeratureTB.Text = "  0 °C";
                humidityTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                humidityTB.Text = "  0 %";
                chargeTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                chargeTB.Text = "   0 V";
            }
        }
    }
}

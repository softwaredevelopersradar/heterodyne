﻿using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
       private BackgroundWorker _worker;
       private BackgroundWorker _worker1;
       private BackgroundWorker _worker2;

        private delegate void UpdateProgressBarDelegate(DependencyProperty dp, object value);
        private delegate void UpdateProgressBarDelegate1(DependencyProperty dp, object value);
        private delegate void UpdateProgressBarDelegate2(DependencyProperty dp, object value);

        ManualResetEvent mrseFWSProgress;
        void worker_DoWork_FUSS(object sender, DoWorkEventArgs e)
        {
            UpdateProgressBarDelegate updProgress = new UpdateProgressBarDelegate(FUSSProgressBar.SetValue);
            double value = 0;
            double Max = 0;
            Dispatcher.Invoke(() => { Max = FUSSProgressBar.Maximum; });


            ShowStateRadioButton(false);
            ShowStateButton(Update, false);
            ShowStateButton(SwitchFrequenceButton, false);

            for (int i = 0; i < Max; i++)
            {
                Thread.Sleep(10);
                Dispatcher.Invoke(updProgress, new object[] { ProgressBar.ValueProperty, ++value });
            }
            Dispatcher.Invoke(() =>
            {
                CheckFUSS.IsChecked = false;
                CheckFWS.IsChecked = false;
                CheckFW.IsChecked = false;
                ShowStatePropertyGrid(true);
                ShowStateButton(Update, true);
                ShowStateButton(SwitchOff, true);
                ShowStateButton(SwitchFrequenceButton, true);
                ShowStateRadioButton(true);
            });
            _comandSendNow = false;
        }

        void worker_DoWork_FWS(object sender, DoWorkEventArgs e)
        {
            UpdateProgressBarDelegate1 updProgress = new UpdateProgressBarDelegate1(FWSProgressBar.SetValue);
            double value = 0;
            double Max = 0;
            Dispatcher.Invoke(() => { Max = FWSProgressBar.Maximum; });

            ShowStateRadioButton(false);
            ShowStateButton(Update, false);
            ShowStateButton(SwitchFrequenceButton, false);

            for (int i = 0; i < Max; i++)
            {
                if (mrseFWSProgress != null)
                    mrseFWSProgress.WaitOne();
                Thread.Sleep(10);               
                Dispatcher.Invoke(updProgress, new object[] { ProgressBar.ValueProperty, ++value });
            }
            Thread.Sleep(100);
            Dispatcher.Invoke(() =>
            {
                ShowStatePropertyGrid(true);
                CheckFUSS.IsChecked = false;
                CheckFWS.IsChecked = false;
                CheckFW.IsChecked = false;
                ShowStateButton(Update, true);
                ShowStateButton(SwitchOff, true);
                ShowStateRadioButton(true);
                ShowStateButton(SwitchFrequenceButton, true);
            });
            _comandSendNow = false;
        }
        void worker_DoWork_FW(object sender, DoWorkEventArgs e)
        {
            UpdateProgressBarDelegate updProgress = new UpdateProgressBarDelegate(FWProgressBar.SetValue);
            double value = 0;
            double Max = 0;
            Dispatcher.Invoke(() => { Max = FWProgressBar.Maximum; });

            ShowStateRadioButton(false);
            ShowStateButton(SwitchFrequenceButton, false);
            ShowStateButton(Update, false);

            for (int i = 0; i < Max; i++)
            {
                Thread.Sleep(10);
                Dispatcher.Invoke(updProgress, new object[] { ProgressBar.ValueProperty, ++value });
            }
            Dispatcher.Invoke(() =>
            {
                ShowStatePropertyGrid(true);
                ShowStateButton(Update, true);
                ShowStateButton(SwitchOff, true);
                CheckFUSS.IsChecked = false;
                CheckFWS.IsChecked = false;
                CheckFW.IsChecked = false;        
                ShowStateRadioButton(true);
                ShowStateButton(SwitchFrequenceButton, true);
            });
            _comandSendNow = false;
        }
        private void InitBackgroundWorker()
        {
            _worker = new BackgroundWorker();
            _worker1 = new BackgroundWorker();
            _worker2 = new BackgroundWorker();

            _worker1.WorkerReportsProgress = true;
            _worker2.WorkerReportsProgress = true;
            _worker.WorkerReportsProgress = true;

            _worker.DoWork += worker_DoWork_FUSS;
            _worker1.DoWork += worker_DoWork_FWS;
            _worker2.DoWork += worker_DoWork_FW;
        }
    }
}

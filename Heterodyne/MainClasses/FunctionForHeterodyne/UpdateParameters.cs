﻿using CNT;
using System.Windows;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (_comHTRD != null)
            {               
                SpecialSend(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
                _initializationFlag = true;
            }
        }

        private void TimerUpdate(object obj)
        {
            if (_comHTRD != null && _comandSendNow == false)
            {
                _comHTRD.Send(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
                DispatchIfNecessary(() => { SendMassageLabel.Text = _len.StatusBar5; });
            }
        }

        public void UpdateComSpeedAndComPort()
        {
            if (_servConected == false)
            {
                InitConnectionHTRD();
            }
            else
            {
                if (_comHTRD != null)
                {
                    _comHTRD.ClosePort();                    
                }
            }
        }
    }
}

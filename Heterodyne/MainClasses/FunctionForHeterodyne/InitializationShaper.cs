﻿using CNT;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private byte _address;
        private DispatcherTimer _timerShaper;

         private void InitConnectionShaper()
         {
             if (_comShaper != null)
                 _comShaper = null;
             _comShaper = new ComShaper();

             _comShaper.OnConnectPort += new ComShaper.ConnectEventHandler(ShowConnectHTRD);
             _comShaper.OnDisconnectPort += new ComShaper.ConnectEventHandler(ShowDisconnectHTRD);

             _comShaper.OnReceiveCmd += new ComShaper.ByteEventHandler(ShowReceiveRequest);

                 //read array of byte
              _comShaper.OnReadByte += new ComShaper.ByteEventHandler(ShowReadByteShaper_My);
                // write array of byte
             _comShaper.OnWriteByte += new ComShaper.ByteEventHandler(WriteByteShaper);

             if (PrGr.MySettings.ComPortCommon != null && PrGr.MySettings.SpeedCommon >= 0)
             {
                 _comShaper.OpenPort(PrGr.MySettings.ComPortCommon,
                   PrGr.MySettings.SpeedCommon, Parity.None, 8, StopBits.One);
             }
         }

        private void ShowReadByteShaper_My(byte[] bData)
         {
             if (!HTRD.Dispatcher.CheckAccess())
             {
                HTRD.Dispatcher.Invoke(() =>
                 {
                     HTRD.ReadDataColor = _green;
                     _tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, _timeIndicate, 0);
                 });
             }
             else
             {
                 HTRD.ReadDataColor = _green;
                 _tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, _timeIndicate, 0);
             }
         }
        private void ShowByteShaper(byte[] bData)
        {
            string mess = "";
            if (bData != null)
            {
                for (int i = 0; i < bData.Length; i++)
                    mess += bData[i].ToString("X2") + " ";
            }
            mess += "\n";
            ShowLogMessageRequest(0, "", mess, "");
        }

         private void ShowLogMessageRequest(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd)
         {
            Brush LogBrushes;
             if (!Dispatcher.CheckAccess())
             {
                 Dispatcher.BeginInvoke(_ShowLogMessage, new object[] { bType, strTimeCodeAdr, strInfoMessage, strTypeCmd });
             }
             else
             {
                 try
                 {

                    switch (bType)
                    {
                        case 0:
                            LogBrushes = Brushes.MediumPurple;
                            break;
                        case 1:
                            LogBrushes = Brushes.Red;
                            break;
                        default:
                            LogBrushes = Brushes.Black;
                            break;
                    }

                    Log.AddTextToLog(strTimeCodeAdr, LogBrushes, _koef);

                    Log.AddTextToLog(strTypeCmd, LogBrushes, _koef);

                    Log.AddTextToLog(strInfoMessage, LogBrushes, _koef);

                    TextPointer caretPos = Log.richTextBox.CaretPosition;
                    caretPos = caretPos.DocumentEnd;
                    Log.richTextBox.CaretPosition = caretPos;
                }
                 catch (System.Exception ex)
                 {
                     MessageBox.Show(ex.Message, "Error");
                 }
                 _timerShaper.Start();
             }
         }

         private void WriteByteShaper(byte[] bData)
           {
            if (!HTRD.Dispatcher.CheckAccess())
               {
                HTRD.Dispatcher.Invoke(() =>
                   {
                       HTRD.WriteDataColor = _green;
                       _tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, _timeIndicate, 0);
                   });
               }
               else
               {
                 HTRD.WriteDataColor = _green;
                 _tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, _timeIndicate, 0);
               }
               ShowWtiteShaper(bData);
           }

           private void ShowWtiteShaper(byte[] bByte)
           {
               string hex = BitConverter.ToString(bByte);
               hex = hex.Replace("-", " ");
               hex += "\n";
               ShowLogMessageRequest(7, hex, "", "");
           }

         private void ShowReceiveRequest(byte[] structure)
         {
             string mess = "";
             if (structure[0] == ComShaper.AddressDispToGen)
             {
                 _address = ComShaper.AddreddGenToDisp;
             }
             else 
             {
                 _address = ComShaper.AddressToComp;
             }
             for (int i = 0; i < structure.Length; i++)
             {
                 mess += structure[i].ToString("X2") + " ";
             }
             ShowLogMessageRequest(4, mess, "", "");
         }
    }
}

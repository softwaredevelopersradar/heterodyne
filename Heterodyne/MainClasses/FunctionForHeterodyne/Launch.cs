﻿using CNT;
using Heterodyne.PropGridClasses;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Heterodyne {
    partial class MainWindow : Window
    {
        private double FUSSCoefficient { get; set; } = 25.0;
        private int FrequencyCoefficient { get; set; } = 1;


        #region Function of Launch
        private void LaunchFUSS()
        {
            if (PrGr.MySettings.AmountOfIterationFUSS >= 0)
            {
                if (PrGr.MySettings.AmountOfSignalPerSecFUSS >= 0)
                {
                    if (PrGr.MySettings.AmountOfSignalPerSecFUSS < _minSaltusPprch)
                        PrGr.MySettings.AmountOfSignalPerSecFUSS = _minSaltusPprch;
                    if (PrGr.MySettings.AmountOfSignalPerSecFUSS > _maxSaltusPprch)
                        PrGr.MySettings.AmountOfSignalPerSecFUSS = _maxSaltusPprch;

                    if (PrGr.MySettings.StepOfFreqencyFUSS >= 0)
                    {
                        if (PrGr.MySettings.StepOfFreqencyFUSS > _maxStepPprch)
                            PrGr.MySettings.StepOfFreqencyFUSS = _maxStepPprch;
                        if (PrGr.MySettings.StepOfFreqencyFUSS < _minStepPprch)
                            PrGr.MySettings.StepOfFreqencyFUSS = _minStepPprch;

                        PrGr.MySettings.StepOfFreqencyFUSS = (Int32)((Math.Round((PrGr.MySettings.StepOfFreqencyFUSS) / FUSSCoefficient)) * FUSSCoefficient);
                        try
                        {
                            Int32 freq = Convert.ToInt32(PrGr.MySettings.FrequencyFUSS * FrequencyCoefficient); 
                            if (PrGr.MySettings.FrequencyFUSS >= 0 && PrGr.MySettings.FrequencyFUSS >= _minFreq && PrGr.MySettings.FrequencyFUSS <= _maxFreq)
                            {

                                FUSSProgressBar.Maximum = PrGr.MySettings.Duration * 100;

                                SpecialSend(freq, ComHTRD.PPRCH,
                                         Convert.ToByte(PrGr.MySettings.AmountOfIterationFUSS),
                                     Convert.ToByte(Math.Round(PrGr.MySettings.StepOfFreqencyFUSS / FUSSCoefficient)),
                                   Convert.ToInt16(PrGr.MySettings.AmountOfSignalPerSecFUSS), Convert.ToByte(PrGr.MySettings.Duration));
                            }
                            else
                            {
                                ErrorTextBox.Text = _len.Error2;
                            }
                        }
                        catch (Exception)
                        {
                            ErrorTextBox.Text = _len.Error1;
                            PrGr.propertyGrid.Properties[2].SetValue((Int32)30000);
                        }
                    }
                    else
                    {
                        ErrorTextBox.Text = _len.Error2;
                    }
                }
                else
                {
                    PrGr.propertyGrid.Properties[1].SetValue((Int32)1);
                    ErrorTextBox.Text = _len.Error4;
                }
            }
            else
            {
                ErrorTextBox.Text = _len.Error5;
            }
        }

        private void LaunchFW()
        {
            if (PrGr.MySettings.StepFW >= 0)
            {
                try
                {
                    string[] selectStrip = (PrGr.MySettings.RangeFW).Split('-');
                    selectStrip[0] = selectStrip[0].Trim();
                    selectStrip[1] = selectStrip[1].Trim();
                    double freq;
                    if (PrGr.MySettings.Offset == true)
                        freq = ((Convert.ToDouble(selectStrip[1] + 0.5) + (Convert.ToDouble(selectStrip[0]) + 0.5)) / 2) * 1000;
                    else freq = ((Convert.ToDouble(selectStrip[1]) + Convert.ToDouble(selectStrip[0])) / 2) * 1000;
                    if (PrGr.MySettings.Offset == true)
                        freq -= (1000 * PrGr.MySettings.StepFW / 2);
                    FWProgressBar.Maximum = PrGr.MySettings.Duration * 100;
                    SpecialSend(Convert.ToInt32(freq), ComHTRD.FielFreq, Convert.ToByte(CalculateNumberOfFrquency()), Convert.ToByte(PrGr.MySettings.StepFW), 0, Convert.ToByte(PrGr.MySettings.Duration));
                }
                catch (Exception)
                {
                    ErrorTextBox.Text = _len.Error1;
                }
            }
        }

        private void LaunchFWS()
        {
            if (PrGr.MySettings.AutoFWS == true)
            {
                if (PrGr.MySettings.FrequencyFWS >= 0)
                {
                    try
                    {
                        if (PrGr.MySettings.FrequencyFWS >= _minFreq && PrGr.MySettings.FrequencyFWS <= _maxFreq)
                        {
                            Int32 freq = Convert.ToInt32(PrGr.MySettings.FrequencyFWS * FrequencyCoefficient);
                            FWSProgressBar.Maximum = PrGr.MySettings.Duration * 100;
                            SpecialSend(freq, ComHTRD.FRCH, 0, 0, 0, Convert.ToByte(PrGr.MySettings.Duration));
                            _firstLaunchNotAutoFWS = true;
                        }
                        else
                        {
                            ErrorTextBox.Text = _len.Error1;
                            PrGr.propertyGrid.Properties[1].SetValue((Int32)30000);
                        }
                    }
                    catch (Exception)
                    {
                        ErrorTextBox.Text = _len.Error1;
                        PrGr.propertyGrid.Properties[1].SetValue((Int32)30000);
                    }
                }
                else
                {
                    ErrorTextBox.Text = _len.Error2;
                }
            }
            else
            {

                if (PrGr.MySettings.FrequencyFWS >= 0)
                {
                    try
                    {
                        if (PrGr.MySettings.StepFWS >= _minFreq && PrGr.MySettings.FrequencyFWS <= _maxFreq)
                        {
                            FWSworker = new Thread(Calculation);
                            mrse = new ManualResetEvent(false);
                            mrseFWSProgress = new ManualResetEvent(false);
                            FWSProgressBar.Maximum = Convert.ToInt32((((PrGr.MySettings.EndFreqencyFWS - PrGr.MySettings.FrequencyFWS) / PrGr.MySettings.StepFWS)+1) * PrGr.MySettings.TimeFWS) * 100;
                            FWSworker.Start();
                        }
                        else
                        {
                            ErrorTextBox.Text = _len.Error1;
                            PrGr.propertyGrid.Properties[1].SetValue((Int32)30000);
                        }
                    }
                    catch (Exception)
                    {
                        ErrorTextBox.Text = _len.Error1;
                        PrGr.propertyGrid.Properties[1].SetValue((Int32)30000);
                    }
                }
                else
                {
                    ErrorTextBox.Text = _len.Error2;
                }
            }
        }
        #endregion

        private int CalculateNumberOfFrquency()
        {
            int numberFileFreq = 0;

            //в зависимости от выбранного диапазона и шага перестройки рассчитываем количество частот
            string[] SelectStrip = PrGr.MySettings.RangeFW.Split('-');
            if (SelectStrip.Length == 2)
            {
                numberFileFreq = (int)((Convert.ToInt32(SelectStrip[1]) - Convert.ToInt32(SelectStrip[0])) / PrGr.MySettings.StepFW);
                if (PrGr.MySettings.RangeFW == PG.Mass[PG.Mass.Length - 1])
                {
                    numberFileFreq += 1;
                }
            }
            else
            {
                ErrorTextBox.Text = _len.Error6;
            }
            return numberFileFreq;
        }

        private void LaunchOneType() {

            if (HTRD.ButServerColor == Brushes.Red)
            { ErrorTextBox.Text = _len.Error9;
                CheckFUSS.IsChecked = false;
                CheckFWS.IsChecked = false;
                CheckFW.IsChecked = false;
                return;
            }

            //if (_comHTRD != null && _comHTRD.PortConnect())
            {
                ShowStateRadioButton(false);
                ShowStateButton(Update, false);
                ShowStateButton(SwitchFrequenceButton, false);


                FWProgressBar.Value = 0;
                FWSProgressBar.Value = 0;
                FUSSProgressBar.Value = 0;
                //if (_flagFirstLaunch == true)
                //{
                  //  Initiliazation();
                //}
                //else
                {
                    Thread.Sleep(200);
                    if (CheckFUSS.IsChecked == true)
                    {
                        FUSSGrid.Visibility = Visibility.Visible;
                        FWSGrid.Visibility = Visibility.Collapsed;
                        FWGrid.Visibility = Visibility.Collapsed;
                        LaunchFUSS();
                    }
                    else if (CheckFWS.IsChecked == true)
                    {
                        FUSSGrid.Visibility = Visibility.Collapsed;
                        FWSGrid.Visibility = Visibility.Visible;
                        FWGrid.Visibility = Visibility.Collapsed;
                        LaunchFWS();
                    }
                    else if (CheckFW.IsChecked == true)
                    {
                        FUSSGrid.Visibility = Visibility.Collapsed;
                        FWSGrid.Visibility = Visibility.Collapsed;
                        FWGrid.Visibility = Visibility.Visible;
                        LaunchFW();
                    }
                }
            }
        }

        private void MainLaunch()
        {
            Dispatcher.Invoke(() =>
            {
                SendMassageLabel.Text = _len.StatusBar5;
                _countMaxEnter = 0;
                _codeMessFromShaper = "";
                //   ErrorTextBox.Text = _len.StatusBar2;
                errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                errorTB.Text = "  No";
                powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                powerTB.Text = "  Yes";
                Thread.Sleep(200);

                ShowStateRadioButton(false);
                ShowStateButton(Update, false);
                ShowStateButton(SwitchFrequenceButton, false);
                if (CheckFUSS.IsChecked == true)
                {
                    FUSSGrid.Visibility = Visibility.Visible;
                    FWSGrid.Visibility = Visibility.Collapsed;
                    FWGrid.Visibility = Visibility.Collapsed;
                    LaunchFUSS();
                }
                else if (CheckFWS.IsChecked == true)
                {
                    FUSSGrid.Visibility = Visibility.Collapsed;
                    FWSGrid.Visibility = Visibility.Visible;
                    FWGrid.Visibility = Visibility.Collapsed;
                    LaunchFWS();
                }
                else if (CheckFW.IsChecked == true)
                {
                    FUSSGrid.Visibility = Visibility.Collapsed;
                    FWSGrid.Visibility = Visibility.Collapsed;
                    FWGrid.Visibility = Visibility.Visible;
                    LaunchFW();
                }
            });
        }

        private void Initiliazation()
        {
            if (_comHTRD != null)
            {
                SpecialSend1();
            }
            else
            {
                MessageBox.Show("Отсутствует подключение");
            }
        }


        ManualResetEvent mrse;
        double PreConvertedFrequency;
        int con;
        bool _NotautoFWSflag = false;
        private void Calculation()
        {
            _firstLaunchNotAutoFWS = true;
            con = 0;
            _NotautoFWSflag = true;
            PreConvertedFrequency = Convert.ToDouble(PrGr.MySettings.FrequencyFWS);
            int time = PrGr.MySettings.TimeFWS;
            time = PrGr.MySettings.TimeFWS;
            for (int i = 0; i < ((PrGr.MySettings.EndFreqencyFWS - PrGr.MySettings.FrequencyFWS) / PrGr.MySettings.StepFWS) + 1 + con; i++)
            {
                int freq = Convert.ToInt32(PreConvertedFrequency * FrequencyCoefficient);
                SpecialSendFWS(freq, ComHTRD.FRCH, 0, 0, 0, Convert.ToByte(PrGr.MySettings.TimeFWS));
                mrse.WaitOne();

                PreConvertedFrequency += PrGr.MySettings.StepFWS;
                if (time == 0)
                    Thread.Sleep(500);
                else
                    Thread.Sleep(time * 1000);
            }
            _NotautoFWSflag = false;
            mrse = null;
            FWSworker.Abort();
        }

        public void Resume()
        {
            try
            {
                mrse.Set();
            }
            catch (Exception) { }

        }

        public void Pause()
        {
            try
            {
                mrse.Reset();
            }
            catch (Exception) { }
        }

        public void Resume1()
        {
            try { mrseFWSProgress.Set(); }
            catch (Exception) { }
        }

        public void Pause1()
        {
            try
            {
                mrseFWSProgress.Reset();
            }
            catch (Exception) { }
        }
    }
}

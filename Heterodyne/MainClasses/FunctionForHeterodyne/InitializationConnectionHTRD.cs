﻿using CNT;
using Heterodyne.PropGridClasses;
using System;
using System.IO.Ports;
using System.Threading;
using System.Windows;
using System.Windows.Media;


namespace Heterodyne
{
    partial class MainWindow : Window
    {
        public delegate void dlgShowLogMessage(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd);
        public dlgShowLogMessage _ShowLogMessage;
        private bool _servConected = false;

        private void InitDelegate()
        {
            _ShowLogMessage = new dlgShowLogMessage(ShowLogMessage);
        }
        private RadialGradientBrush _green;

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void InitConnectionHTRD()
        {
            if (_comHTRD != null)
                _comHTRD = null;
            _comHTRD = new ComHTRD();

            _comHTRD.OnConnectPort += new ComHTRD.ConnectEventHandler(ShowConnectHTRD);
            _comHTRD.OnDisconnectPort += new ComHTRD.ConnectEventHandler(ShowDisconnectHTRD);
            _comHTRD.OnReceiveCmd += new ComHTRD.CmdEventHandler(ShowReceiveCmd);
            // write array of byte
            _comHTRD.OnWriteByte += new ComHTRD.ByteEventHandler(ShowWriteByteHTRD);
            // read array of byte
            _comHTRD.OnReadByte += new ComHTRD.ByteEventHandler(ShowReadByteHTRD_My);
           
            if (PrGr.MySettings.ComPortCommon != null && PrGr.MySettings.SpeedCommon > 0)
            {
                 _comHTRD.OpenPort(PrGr.MySettings.ComPortCommon,
                    PrGr.MySettings.SpeedCommon, Parity.None, 8, StopBits.One);
            }
            try
            {
                _timeSend = Convert.ToUInt16(PG.ini.IniReadValue("Poll", "TimeSend"));
                _maxCountReq = Convert.ToInt16(PG.ini.IniReadValue("Poll", "MaxCountReq"));
                _pollTimeDelay = Convert.ToUInt16(PG.ini.IniReadValue("Poll", "TimeDelay"));
                _timeSend_FrequenceChanged = Convert.ToUInt16(PG.ini.IniReadValue("Poll", "TimeSend_SwitchingFrequence"));
            }
            catch(Exception)
            {
                _timeSend = 1000;
                _maxCountReq = 5;
                _pollTimeDelay = 1000;
                _timeSend_FrequenceChanged = 1100;
            }
            DispatchIfNecessary(() => { HTRD.ShowRead(); });
            DispatchIfNecessary(() => { HTRD.ShowWrite(); });
        }

        private void ShowWriteByteHTRD(byte[] bData)
        {
            try
            {             
               DispatchIfNecessary(() => { HTRD.ShowWrite(); });
            }
            catch (Exception) { DispatchIfNecessary(() => { ErrorTextBox.Text = "System Exeption"; }); }          
            ShowWtiteDByte(bData);
        }

        private void ShowWtiteDByte(byte[] bByte)
        {
            string hex = BitConverter.ToString(bByte);
            hex = hex.Replace("-", " ");
            hex += "\n";
            ShowLogMessage(2, hex, "", "");
        }

        private void ShowConnectHTRD()
        {
            HTRD.ShowConnect();
            ShowStateRadioButton(false);
            ShowStatePropertyGrid(true);
            ShowStateButton(Update, true);
            ShowStateButton(SwitchOff, false);
            ShowStateButton(SwitchFrequenceButton, true);
            _timerOpen.Start();
            _timerInitialization.Stop();
            _servConected = true;
            tm = new TimerCallback(TimerUpdate);
            UpdateParameters = new Timer(tm, 0, 3000, 15000);
        }

        private void ShowDisconnectHTRD()
        {
            HTRD.ShowDisconnect();
            ShowStateGrid();
            ShowStateDataTextBox();
            _timerInitialization.Start();
            _initializationFlag = false;
            _flagFirstLaunch = true;
            _servConected = false;
        }

        private void ShowReadByteHTRD_My(byte[] bData)
        {
           try
           {
                DispatchIfNecessary(() => {HTRD.ShowRead();});
                Thread.Sleep(50);
           }
            catch (SystemException) {
                DispatchIfNecessary(() => { ErrorTextBox.Text = "System Exeption"; });
           }         
            ShowByteHTRD(bData);
        }

        private void ShowByteHTRD(byte[] bData)
        {
            string mess = "";
            if (bData != null)
            {
                for (int i = 0; i < bData.Length; i++)
                    mess += bData[i].ToString("X2") + " ";
            }
            mess += "\n";

            if(Log.ShowAllByte == true)
                ShowLogMessage(0, "", mess, "");
        }

        private void ShowLogMessage(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd)
        {
            char[] m = new char[2] { ' ', '\n' };
            if (!this.Dispatcher.CheckAccess())
            {
               this.Dispatcher.BeginInvoke(_ShowLogMessage, new object[] { bType, strTimeCodeAdr, strInfoMessage, strTypeCmd });
            }
            else
            {
                try
                {
                    if (strInfoMessage.Equals(strTypeCmd))
                        strTypeCmd = "";
                    if (strTimeCodeAdr.Equals(strInfoMessage))
                        strInfoMessage = "";
                    if (strTypeCmd.Equals(strTimeCodeAdr))
                        strTimeCodeAdr = "";
                    switch (bType)
                    {
                        case 0:
                            if (strTimeCodeAdr.Trim() != "")
                                Log.AddTextToLog(strTimeCodeAdr.Trim(m) + "\n", Brushes.MediumPurple, _koef);
                            break;
                        case 1:
                            if (strTimeCodeAdr.Trim() != "")
                                Log.AddTextToLog(strTimeCodeAdr.Trim(m) + "\n", Brushes.Red, _koef);
                            break;
                        default:
                            if (strTimeCodeAdr.Trim() != "")
                                Log.AddTextToLog(strTimeCodeAdr.Trim(m) + "\n", Brushes.LightGray, _koef);
                            break;
                    }

                    if (strTypeCmd.Trim() != "")
                        Log.AddTextToLog(strTypeCmd.Trim(m) + "\n", Brushes.LightGray, _koef);

                    switch (bType)
                    {
                        case 0:
                            if (strInfoMessage.Trim() != "")
                                Log.AddTextToLog(strInfoMessage.Trim(m) + "\n", Brushes.MediumPurple, _koef);
                            break;
                        case 1:
                            if (strInfoMessage.Trim() != "")
                                Log.AddTextToLog(strInfoMessage.Trim(m) + "\n", Brushes.Red, _koef);
                            break;
                        default:
                            if (strInfoMessage.Trim() != "")
                                Log.AddTextToLog(strInfoMessage.Trim(m) + "\n", Brushes.LightGray, _koef);
                            break;
                    }
                    
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private bool _firstLaunchNotAutoFWS = true;

        private void ShowReceiveCmd(object structure)
        {
           
            Answer partAnswer = (Answer)structure;

          
            try
             {
                LastPartAnswer lastPart = (LastPartAnswer)partAnswer.obj;
                 if (lastPart.ErrorCode != 0x04)
                 {
                   
                    if (_tmAutoEnterFWS != null)
                    {
                        DispatchIfNecessary(() => { _tmAutoEnterFWS.Dispose(); });
                    }
                    if (_tmAutoEnterFWS != null)
                    { Resume();
                        Resume1();
                    }
                    if (_tmAutoEnter != null)
                    {
                        DispatchIfNecessary(() => { _tmAutoEnter.Dispose(); });
                    }
                    if(_tmAutoEnterSwitchingFreq != null)
                    {
                        DispatchIfNecessary(() => { _tmAutoEnterSwitchingFreq.Dispose(); });
                    }
                    _countMaxEnter = 0;
                     ShowStateButton(SwitchOff, true);
                    //ShowStateRadioButton(true);
                    //(Update, false);
                    _stateSend = true;
                    DispatchIfNecessary(() => { SendMassageLabel.Text = _len.StatusBar1; });
                }
             }
             catch(InvalidCastException)
            {
               
            }
            catch (Exception)
            {
                
                NoAnswerAction();
            }

           


            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            string mess = "";
            string messError = "";
            string message = "";

           // DispatchIfNecessary(() => { ErrorTextBox.Text = _len.StatusBar2; });
       

            switch (partAnswer.mainFiel.cipher)
            {
                case ComHTRD.FRCH:
                    #region FRCH   

                    if (_tmAutoEnterFWS != null)
                    {                 
                        DispatchIfNecessary(() => { _tmAutoEnterFWS.Dispose(); });                   
                    }
                    if (_tmAutoEnterFWS != null)
                    { Resume();
                        Resume1();
                    }
                        if (_tmAutoEnter != null)
                    {
                        DispatchIfNecessary(() => { _tmAutoEnter.Dispose(); });
                    }

                    try
                    {
                        if (_firstLaunchNotAutoFWS == true)
                        {
                            Dispatcher.Invoke(() => { _worker1.RunWorkerAsync(); });
                            _firstLaunchNotAutoFWS = false;
                        }
                    }catch (Exception) { }

                    LastPartAnswer answerFM = (LastPartAnswer)partAnswer.obj;

                    // FreqPoll(answerFM.ErrorCode);
                   
                    byte[] crcFM = BitConverter.GetBytes(answerFM.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answerFM.ErrorCode.ToString("X2") + " " +
                           crcFM[0].ToString("X2") + " " + crcFM[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, ""); 
                    messError = FindError(answerFM);
                    _codeMessFromShaper = answerFM.ErrorCode.ToString("X2");
                    ShowInfoLabel(ErrorTextBox, messError);
                    if (answerFM.ErrorCode != 0x00)
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = Brushes.Red;
                                errorTB.Text = "  Yes";});

                        DispatchIfNecessary(() =>
                        {
                            ErrorTextBox.Text = FindError(answerFM);
                        });
                    }
                    else
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                                errorTB.Text = "  No";
                            });
                        if (answerFM.ErrorCode == 0x00 && FlagOFF == false)
                        {
                            Dispatcher.Invoke(() => {powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                                powerTB.Text = "  Yes";
                            });
                            if (PrGr.MySettings.Duration != 0)
                                _tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(PrGr.MySettings.Duration) * 1000, 0);
                        }
                        else
                        {
                            Dispatcher.Invoke(() => { powerTB.Foreground = Brushes.Red;
                                powerTB.Text = "  No";
                            });
                            FlagOFF = false;
                        }
                    }
                 
                    #endregion
                    break;

                case ComHTRD.OffSignal:
                    #region OffSignal
                    if(_tmAutoEnter != null)
                    DispatchIfNecessary(() => { _tmAutoEnter.Dispose(); });

                    LastPartAnswer answer = (LastPartAnswer)partAnswer.obj;
                    byte[] crc = BitConverter.GetBytes(answer.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answer.ErrorCode.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");

                    if (answer.ErrorCode == _comHTRD.listError[0] && FlagOFF == true)
                    {
                        messError = _len.MessError1;
                        _codeMessFromShaper = "Mess_OFF";
                    }
                    else
                    {
                        messError = FindError(answer);
                        _codeMessFromShaper = answer.ErrorCode.ToString("X2");
                    }

                    ShowInfoLabel(ErrorTextBox, messError);

                    if (answer.ErrorCode != 0x00)
                    {
                        DispatchIfNecessary(() =>
                        {
                            errorTB.Foreground = Brushes.Red;
                            errorTB.Text = "  Yes";
                        });
                        DispatchIfNecessary(() => { ErrorTextBox.Text = FindError(answer); });                      
                    }
                    else
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                                errorTB.Text = "  No";
                            });
                    }
                    if (answer.ErrorCode == 0x00 && FlagOFF == false)
                    {
                        Dispatcher.Invoke(() => {
                            powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                            powerTB.Text = "  Yes";
                        });
                        if (PrGr.MySettings.Duration != 0)
                            _tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(PrGr.MySettings.Duration) * 1000, 0);
                    }
                    else
                    {
                        Dispatcher.Invoke(() => {
                            powerTB.Foreground = Brushes.Red;
                            powerTB.Text = "  No";
                        });
                        FlagOFF = false;
                    }

                    ShowStateButton(Update, true);
                    ShowStateRadioButton(true);
                    ShowStateButton(SwitchOff, true);
                    ShowStateButton(SwitchFrequenceButton, true);
                    ShowStatePropertyGrid(true);

                    #endregion
                    break;

                case ComHTRD.ChangeFreq:
                    if (_tmAutoEnterSwitchingFreq != null)
                    {
                        DispatchIfNecessary(() => { _tmAutoEnterSwitchingFreq.Dispose(); });
                    }
                    SwitchingPartAnswer answer5 = (SwitchingPartAnswer)partAnswer.obj;
                    byte[] crc5 = BitConverter.GetBytes(answer5.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answer5.SwitchingFreq.ToString("X2") + " " +
                           crc5[0].ToString("X2") + " " + crc5[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");
                    _comandSendNow = false;

                    ShowStateButton(Update, true);
                    ShowStateRadioButton(true);
                    ShowStateButton(SwitchOff, true);
                    ShowStateButton(SwitchFrequenceButton, true);
                    ShowStatePropertyGrid(true);
                    break;

                case ComHTRD.FM:
                case ComHTRD.AM:
                case ComHTRD.CHM:
                case ComHTRD.FielFreq:
                    #region FileFreq
          
                    if (_tmAutoEnter != null)
                        DispatchIfNecessary(() => { _tmAutoEnter.Dispose(); });
                    try
                    {
                        Dispatcher.Invoke(() => { _worker2.RunWorkerAsync(); });
                    }
                    catch (Exception) { }
                  

                    LastPartAnswer answer1 = (LastPartAnswer)partAnswer.obj;
                    byte[] crc1 = BitConverter.GetBytes(answer1.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answer1.ErrorCode.ToString("X2") + " " +
                           crc1[0].ToString("X2") + " " + crc1[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");

                    if (answer1.ErrorCode == _comHTRD.listError[0] && FlagOFF == true)
                    {
                        messError = _len.MessError1;
                        _codeMessFromShaper = "Mess_OFF";
                    }
                    else
                    {
                        messError = FindError(answer1);
                        _codeMessFromShaper = answer1.ErrorCode.ToString("X2");
                    }

                    ShowInfoLabel(ErrorTextBox, messError);

                    if (answer1.ErrorCode != 0x00)
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = Brushes.Red;
                                errorTB.Text = "  Yes"; });

                        DispatchIfNecessary(() => { ErrorTextBox.Text = FindError(answer1); });
                    }
                    else
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                                errorTB.Text = "  No"; });
                    }
                    if (answer1.ErrorCode == 0x00 && FlagOFF == false)
                    {
                        Dispatcher.Invoke(() => {
                            powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                            powerTB.Text = "  Yes";// + _len.LErrorTB;
                        });
                        if (PrGr.MySettings.Duration != 0)
                            _tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(PrGr.MySettings.Duration) * 1000, 0);
                    }
                    else
                    {
                        Dispatcher.Invoke(() => {
                            powerTB.Foreground = Brushes.Red;
                            powerTB.Text = "  No";// + _len.LErrorTB1;
                        });
                        FlagOFF = false;
                    }
                    #endregion
                    break;

                case ComHTRD.PPRCH:
                    #region PPRCH
                    if (_tmAutoEnter != null)
                        Dispatcher.Invoke(() => _tmAutoEnter.Dispose());
                    try
                    {
                        Dispatcher.Invoke(() => { _worker.RunWorkerAsync(); });
                    }
                    catch (Exception) { }
                    LastPartAnswer answerPPRCH = (LastPartAnswer)partAnswer.obj;
                    byte[] crcPPRCH = BitConverter.GetBytes(answerPPRCH.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answerPPRCH.ErrorCode.ToString("X2") + " " +
                           crcPPRCH[0].ToString("X2") + " " + crcPPRCH[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");

                    if (answerPPRCH.ErrorCode == _comHTRD.listError[0] && FlagOFF == true)
                    {
                        messError = _len.MessError1;
                        _codeMessFromShaper = "Mess_OFF";
                    }
                    else
                    {
                        messError = FindError(answerPPRCH);
                        _codeMessFromShaper = answerPPRCH.ErrorCode.ToString("X2");
                    }
        
                    ShowInfoLabel(ErrorTextBox, messError);

                    if (answerPPRCH.ErrorCode != 0x00)
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = Brushes.Red;
                                errorTB.Text = "  Yes"; });

                        DispatchIfNecessary(() => {     ErrorTextBox.Text = FindError(answerPPRCH); });
                    }
                    else
                    {
                        DispatchIfNecessary(() => {
                            errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                                errorTB.Text = "  No";  });                  
                    }
                    if (answerPPRCH.ErrorCode == 0x00 && FlagOFF == false)
                    {
                        Dispatcher.Invoke(() => { powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                            powerTB.Text = "  Yes";// + _len.LErrorTB;
                        });
                        if (PrGr.MySettings.Duration != 0)
                            _tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(PrGr.MySettings.Duration) * 1000, 0);
                    }
                    else
                    {
                        Dispatcher.Invoke(() => { powerTB.Foreground = Brushes.Red;
                            powerTB.Text = "  No";// + _len.LErrorTB1;
                        });
                        FlagOFF = false;
                    }
                    #endregion
                    break;

                case ComHTRD.SpecialCipher:
                    #region SpecialCipher
                    if (_tmAutoEnter != null)
                        DispatchIfNecessary(() => { _tmAutoEnter.Dispose(); });
                    _stateSend = true;
                    SpecialPartAnswer specialAnswer = (SpecialPartAnswer)partAnswer.obj;
                    crc = new byte[2];
                    crc = BitConverter.GetBytes(specialAnswer.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + specialAnswer.T.ToString("X2") + " " +
                           specialAnswer.H.ToString("X2") + " " + specialAnswer.Charge.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");

                    message = "  " + specialAnswer.T.ToString() + " °C";
                    _temp =  specialAnswer.T.ToString() + " °C";
                    ShowInfoLabel(temeratureTB, message);

                    message = "  " + specialAnswer.H.ToString() + " %";
                    _humidid = specialAnswer.H.ToString() + " %";
                    ShowInfoLabel(humidityTB, message);

                    message = "  " + (ADC_battery(specialAnswer.Charge)).ToString() + " V";
                    _charge = (ADC_battery(specialAnswer.Charge)).ToString() + " V";
                 
                    if(Convert.ToDouble(specialAnswer.T.ToString()) >= 45)
                    {
                        ShowStateLabel(temeratureTB, Brushes.Red);
                    }
                    else
                    {
                        ShowStateLabel(temeratureTB, (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"]);
                    }

                    if (ADC_battery(specialAnswer.Charge) <= 13.5)
                    {
                        ShowStateLabel(chargeTB, Brushes.Red);
                    }
                    else
                    {
                        ShowStateLabel(chargeTB, (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"]);
                    }
                    ShowInfoLabel(chargeTB, message);

                    ShowStateRadioButton(true);
                    ShowStateButton(SwitchOff, true);

                    Dispatcher.Invoke(() => { SendMassageLabel.Text = _len.StatusBar1; });
                    Dispatcher.Invoke(() => { ErrorTextBox.Text = _len.StatusBar2; });
                    if(_initializationFlag == true)
                         ShowObject();
                    ShowStateRadioButton(true);
                    ShowStateButton(SwitchOff, true);
                    _initializationFlag = true;
                    _comandSendNow = false;
                    #endregion
                    break;
            }           
        }

        void FreqPoll(byte error) // функция автоматического опроса
        {
            if (error == 0x00)
            {
                     byte time;
                     try { time = Convert.ToByte(PG.ini.IniReadValue("Poll", "Duration")); }
                     catch { time = 5; }
                     SpecialSend(_currentFreq, ComHTRD.FRCH, 0, 0, 0, time);
            }
        }
    }

}



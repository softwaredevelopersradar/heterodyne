﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;
using System.IO.Ports;
using Heterodyne.PropGridClasses;
using CNT;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private Timer _tmWriteByteHTRD;
        private Timer _tmReadByteHTRD;
        private Timer _tmDurationRadiationHTRD;
        private Timer _tmAutoEnter;
        private Timer _tmAutoEnterFWS;
        private Timer _tmAutoEnterSwitchingFreq;

        private DispatcherTimer _timerInitialization;
        private DispatcherTimer _timerOpen;

        RadialGradientBrush _gray = new RadialGradientBrush(Color.FromRgb(255, 255, 255), Color.FromRgb(104, 104, 104));

        private void TimeWriteByteHTRD(object o)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {                
                    ShowStateRadioButton(true);
                    ShowStateButton(SwitchOff, true);
                    ShowStatePropertyGrid(true);
                    ShowStateButton(Update, true);
                    ShowStateButton(SwitchFrequenceButton, true);
                    _tmWriteByteHTRD.Dispose();                 
                });
            }
            else
            {
                ShowStateRadioButton(true);
                ShowStateButton(SwitchOff, true);
                ShowStatePropertyGrid(true);
                ShowStateButton(Update, true);
                ShowStateButton(SwitchFrequenceButton, true);
                _tmWriteByteHTRD.Dispose();
            }
        }

        private void TimeReadByteHTRD(object o)
        {           
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    ShowStateRadioButton(true);
                    ShowStatePropertyGrid(true);
                    ShowStateButton(Update, true);
                    ShowStateButton(SwitchOff, true);
                    ShowStateButton(SwitchFrequenceButton, true);
                    _tmReadByteHTRD.Dispose();
                });
            }
            else
            {
                ShowStateRadioButton(true);
                ShowStatePropertyGrid(true);
                ShowStateButton(Update, true);
                ShowStateButton(SwitchFrequenceButton, true);
                ShowStateButton(SwitchOff, true);
                _tmReadByteHTRD.Dispose();
            }
        }
        private void TimeDurationRadiationHTRD(object o)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    powerTB.Foreground = Brushes.Red;
                    powerTB.Text = "  No";
                    _tmDurationRadiationHTRD.Dispose();
                });
            }
            else
            {
                powerTB.Foreground = Brushes.Red;
                powerTB.Text = "  No";
                _tmDurationRadiationHTRD.Dispose();
            }
        }

        private void InitTimers()
        {
             PG.ini = new IniFile(@"textResources\SettingsHTRD.ini");

            _timerOpen = new DispatcherTimer();
            _timerInitialization = new DispatcherTimer();
            try
            {
                _timerOpen.Interval = new TimeSpan(Convert.ToInt32(PG.ini.IniReadValue("Parameters", "timerOpen")));
                _timerInitialization.Interval = new TimeSpan(Convert.ToInt32(PG.ini.IniReadValue("Parameters", "timerInitialization")));
                _timeSend = Convert.ToUInt16(PG.ini.IniReadValue("Parameters", "TimeEnter"));
                _timeSend_FrequenceChanged = Convert.ToUInt16(PG.ini.IniReadValue("Poll", "TimeSend_SwitchingFrequence"));

            }
            catch(Exception)
            {
                System.Windows.MessageBox.Show("Check ini file \"SettingsHTRD\"" + System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + @"\textResources\SettingsHTRD.ini");
                _timerOpen.Interval = new TimeSpan(1000);            
                _timerInitialization.Interval = new TimeSpan(1000);
                _timeSend = 1000;
                _timeSend_FrequenceChanged = 1100;
            }
            
            _timerOpen.Tick += new EventHandler(timerOpen_Tick);
            _timerInitialization.Tick += new EventHandler(timerInitialization_Tick);           
            _timerInitialization.Start();
            _timerOpen.Start();            
        }

        private void timerInitialization_Tick(object sender, EventArgs e)
        {
            string[] portnames = SerialPort.GetPortNames();
            int index = Array.IndexOf(portnames, PrGr.MySettings.ComPortCommon);
            if (index > 0)
            {
                InitConnectionHTRD();
                _timerInitialization.Stop();
            }
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            string[] portnames = SerialPort.GetPortNames();
            int index = Array.IndexOf(portnames, PrGr.MySettings.ComPortCommon);
            if (index < 0)
            {
                ShowDisconnectHTRD();
            }
        }

        private string FindError(LastPartAnswer answer)
        {
            string messError = "";
            if (Properties.Settings.Default.Language == "ru-RU")
                messError = _comHTRD.FindErr(answer.ErrorCode);
            if (Properties.Settings.Default.Language == "en-EN")
            {
                switch (answer.ErrorCode)
                {
                    case 0x00:
                        messError = _len.StatusBar2;
                        break;
                    case 0x01:
                        messError = _len.HetrError1;
                        break;
                    case 0x02:
                        messError = _len.HetrError2;
                        break;
                    case 0x03:
                        messError = _len.HetrError3;
                        break;
                    case 0x04:
                        messError = _len.HetrError4;
                        break;
                    case 0x05:
                        messError = _len.HetrError5;
                        break;
                    case 0x0D:
                        messError = _len.HetrErrorD;
                        break;
                    case 0x10:
                        messError = _len.HetrError10; //????????????????PR???????????
                        break;
                    case 0x11:
                        messError = _len.HetrError11; //????????????????PRD???????????
                        break;
                }
            }
            return messError;
        }    

        private double ADC_battery(byte charge)
        {
            double adc_battery = (16.8d * charge) / 155.0d;
            return Math.Round(adc_battery, 2);
        }
    }
}
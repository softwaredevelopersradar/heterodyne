﻿using CNT;
using Heterodyne.MainClasses;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private ItemMW _item;
        private Thread tr;
        private bool _initializationFlag;
        private bool _comandSendNow;
        TimerCallback tm;
        Timer UpdateParameters;

        public void SpecialSend1()
        {
               _comandSendNow = true;
               _stateSend = false;
            tr = new Thread(new ThreadStart(MainLaunch));

            new Action(() =>
            {
                for (int i = 1; i < _maxCountReq+1; i++)
                {
                    if(_initializationFlag == false)
                    _comHTRD.Send(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
                  

                    if (_initializationFlag == true)
                    {
                        _flagFirstLaunch = false;
                        tr.Start();
                        break;
                    }
                                      
                    if (_stateSend == true)
                    {
                        tr.Start();
                        _initializationFlag = true;
                        _flagFirstLaunch = false;
                        break;
                    }
                    Thread.Sleep(1000);
                    if (i >= _maxCountReq)
                    {
                        NoAnswerAction();
                        ShowStateDataTextBox();
                        ShowChackedRadioButtonFalse();
                        _flagFirstLaunch = true;
                        _initializationFlag = false;
                      //  _comandSendNow = false;
                    }
                }
            }).BeginInvoke(null, null);
        }


        private void notShowObject()
        {
            ShowStateButton(Update, false);
            ShowStateRadioButton(false);
            ShowStateButton(SwitchOff, false);
            ShowStateButton(SwitchFrequenceButton, false);
            ShowStatePropertyGrid(false);
        }
        private void ShowObject()
        {
            ShowStateButton(Update, true);
            ShowStateRadioButton(false);
            ShowStateButton(SwitchOff, false);
            ShowStateButton(SwitchFrequenceButton, true);
            ShowStatePropertyGrid(true);
        }

        private void NoAnswerAction()
        {
            Dispatcher.Invoke(() => { SendMassageLabel.Text = _len.StatusBar5; });
            Dispatcher.Invoke(() => { ErrorTextBox.Text = _len.StatusBar4; });
            ShowObject();
        }

        public void SpecialSend(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            notShowObject();
            _countMaxEnter = 0;
            _comandSendNow = true;
            _item = new ItemMW(frequency, Code, numberFreq, step, saltusFreq, time);
            _tmAutoEnter = new Timer(TimeAutoSentByteHTRD, _item, _timeDelay, _timeSend);
        }

        private void TimeAutoSentByteHTRD(object o)
        {
            _comHTRD.Send(_item.Frequency, _item.Code, _item.NumberFreq, _item.Step, _item.SaltusFreq, _item.Time);
            _countMaxEnter++;
      
            if (_countMaxEnter >= _maxCountReq)
            {
                _tmAutoEnter.Dispose();
                NoAnswerAction();
                _countMaxEnter = 0;
                _flagFirstLaunch = true;
                if (_initializationFlag == false)
                    ShowStateDataTextBox();
                _initializationFlag = false;
                ShowChackedRadioButtonFalse();
                _comandSendNow = false;
                return;
            }
        }


        public void SpecialSendSwitchingCommand(FrequenceForSwitching frequence)
        {
            notShowObject();
            _comandSendNow = true;
            _countMaxEnter = 0;
            _tmAutoEnterSwitchingFreq = new Timer(TimeAutoSentByteHTRDSwitching, frequence, _timeDelay, _timeSend_FrequenceChanged);
        }

        private void TimeAutoSentByteHTRDSwitching(object o)
        {
            _comHTRD.SendFreqSwitchingCommand((FrequenceForSwitching)o);
            _countMaxEnter++;

            if (_countMaxEnter >= _maxCountReq)
            {
                _tmAutoEnterSwitchingFreq.Dispose();
                NoAnswerAction();
                _countMaxEnter = 0;
                _flagFirstLaunch = true;
                if (_initializationFlag == false)
                    ShowStateDataTextBox();
                _initializationFlag = false;
                ShowChackedRadioButtonFalse();
                _comandSendNow = false;
                return;
            }
        }


        public void SpecialSendFWS(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            _comandSendNow = true;
            _item = new ItemMW(frequency, Code, numberFreq, step, saltusFreq, time);
             Pause();
    
            _tmAutoEnterFWS = new System.Threading.Timer(TimeAutoSentByteHTRD_FWS, _item, 100, 1150);
        }

        public void SpecialSendFWS_2(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            _comandSendNow = true;
            _item = new ItemMW(frequency, Code, numberFreq, step, saltusFreq, time);
            Pause();
            _tmAutoEnterFWS = new System.Threading.Timer(TimeAutoSentByteHTRD_FWS, _item, 500, 500);
        }

        private void TimeAutoSentByteHTRD_FWS(object o)
        {
            _comHTRD.Send(_item.Frequency, _item.Code, _item.NumberFreq, _item.Step, _item.SaltusFreq, _item.Time);
            Pause1();
            _countMaxEnter++;

            if (_countMaxEnter >= 5)
            {
                _tmAutoEnterFWS.Dispose();
                _countMaxEnter = 0;
                DispatchIfNecessary(() =>{ Cont.Visibility = Visibility.Visible; });
                Pause();             
                Dispatcher.Invoke(() => { SendMassageLabel.Text = _len.StatusBar5; });            
            }
        }

    }
}

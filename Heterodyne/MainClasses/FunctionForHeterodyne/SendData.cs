﻿using Heterodyne.MainClasses;
using Heterodyne.PropGridClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private ItemMW _item;
        private Thread tr;
        public void SpecialSend1(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            _stateSend = false;
            new Action(() =>
            {
                for (int i = 1; i < 8; i++)
                {          
                    _comHTRD.Send(frequency, Code, numberFreq, step, saltusFreq, time);
                    Thread.Sleep(1000);
                    if (_stateSend == true)
                    {
                        _flagFirstLaunch = false;
                        tr.Start();
                        break;
                    }
                    if (i == 7)
                    {
                        Dispatcher.Invoke(() => { SendMassageLabel.Text = "Heterodyne is not responding"; });
                        ShowStateButton(Update, true);
                        ShowStateRadioButton(true);
                        ShowStatePropertyGrid(true);
                        ShowCheckedRadioButton();
                        _stateSend = false;
                        break;
                    }
                }
            }).BeginInvoke(null, null);
        }

        public void SpecialSend(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            _item = new ItemMW(frequency, Code, numberFreq, step, saltusFreq, time);
            _tmAutoEnter = new System.Threading.Timer(TimeAutoSentByteHTRD, _item, _timeDelay, _timeSend);
        }

        private void TimeAutoSentByteHTRD(object o)
        {
            _comHTRD.Send(_item.Frequency, _item.Code, _item.NumberFreq, _item.Step, _item.SaltusFreq, _item.Time);
            _countMaxEnter++;
            if (_countMaxEnter >= _maxCountReq)
            {
                Dispatcher.Invoke(() => { SendMassageLabel.Text = "Heterodyne is not responding"; });
                ShowStateButton(Update, true);
                ShowStateRadioButton(true);
                ShowStatePropertyGrid(true);
                _countMaxEnter = 0;
                _tmAutoEnter.Dispose();
            }
        }

        private void MainLaunch()
        {
            Dispatcher.Invoke(() =>
            {
              //  SendMassageLabel.Text = "Heterodyne is not responding";
                _countMaxEnter = 0;
                _codeMessFromShaper = "";
                ErrorTextBox.Text = _len.StatusBar2;
                errorTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                errorTB.Text = "  No";
                powerTB.Foreground = (LinearGradientBrush)Application.Current.Resources["ControlActiveBorderBrush"];
                powerTB.Text = "  Yes";

                if (CheckFUSS.IsChecked == true)
                {
                    LineCommand.Update(PrGr.MySettings.LineFW);
                    FUSSGrid.Visibility = Visibility.Visible;
                    FWSGrid.Visibility = Visibility.Collapsed;
                    FWGrid.Visibility = Visibility.Collapsed;
                    LaunchFUSS();
                }
                else if (CheckFWS.IsChecked == true)
                {
                    //FWSPB.Maximum = PrGr.MySettings.TimeFWS;
                    FUSSGrid.Visibility = Visibility.Collapsed;
                    FWSGrid.Visibility = Visibility.Visible;
                    FWGrid.Visibility = Visibility.Collapsed;
                    LaunchFWS();
                }
                else if (CheckFW.IsChecked == true)
                {
                    FUSSGrid.Visibility = Visibility.Collapsed;
                    FWSGrid.Visibility = Visibility.Collapsed;
                    FWGrid.Visibility = Visibility.Visible;
                    LaunchFW();
                }
            });
        }

    }
}

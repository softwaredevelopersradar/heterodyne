﻿using Heterodyne.Localization;
using Heterodyne.PropGridClasses;
using System;
using System.Windows;
using System.Windows.Data;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private bool _languageWasLoaded;
      
        private void ConectLocalization(Language1 Len)
        {
            var languageProvider = (ObjectDataProvider)Resources["language"];
            if (!_languageWasLoaded)
            {
                languageProvider.ObjectType = null;
                _languageWasLoaded = true;
            }
            languageProvider.ObjectInstance = Len;
        }

        private void InitLocalization()
        {
            _list1 = Language1.Load();
            try
            {
                if (Properties.Settings.Default.Language == "en-EN")
                {
                    _len = _list1[0]; 
                    Log.Clear.Content = "Clear";
                    Log.allByte.Content = "All bytes";
                    ColExpPG.ToolTip = "Minimize Input Panel";
                    ColExpLog.ToolTip = "Minimize log";
                }
                else if (Properties.Settings.Default.Language == "ru-RU")
                {
                    _len = _list1[1]; 
                    Log.Clear.Content = "Очистить";
                    Log.allByte.Content = "Все байты";
                    ColExpPG.ToolTip = "Свернуть панель ввода";
                    ColExpLog.ToolTip = "Свернуть лог";
                }
                ConectLocalization(_len);
                if (Convert.ToInt32(PG.ini.IniReadValue("Parameters", "frequency")) == 0)
                    SwitchFrequenceButton.Content = _len.Switch2;
                if (Convert.ToInt32(PG.ini.IniReadValue("Parameters", "frequency")) == 1)
                    SwitchFrequenceButton.Content = _len.Switch1;
            }
            catch(Exception)
            {
                _len = _list1[0]; 
                Log.Clear.Content = "Clear";
                Log.allByte.Content = "All bytes";
                ConectLocalization(_len);
            }
        }
    }
}

﻿using System;

namespace Heterodyne.MainClasses
{
    class ItemMW
    {
        public Int32 Frequency;
        public byte Code;
        public byte NumberFreq;
        public byte Step;
        public Int16 SaltusFreq;
        public byte Time;

        public ItemMW(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            this.Frequency = frequency;
            this.Code = Code;
            this.NumberFreq = numberFreq;
            this.Step = step;
            this.SaltusFreq = saltusFreq;
            this.Time = time;
        }
    }
}

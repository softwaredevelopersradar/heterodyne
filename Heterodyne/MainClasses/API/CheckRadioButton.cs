﻿using Heterodyne.PropGridClasses;
using System;
using System.Windows;

namespace Heterodyne
{
    partial class MainWindow : Window
    {


        private void CheckFWS_Checked(object sender, RoutedEventArgs e)
        {

            string temp = "";//CheckWriteZero(PrGr.MySettings.FrequencyFWS);
            if (PrGr.MySettings.AutoFWS == true)
            {
                FreqFWSLabel.Content = Convert.ToString((Double)PrGr.MySettings.FrequencyFWS / 1000) + temp + _len.Text1;
                StepFUSSLabel.Content = "";
                TimeFUSSLabel.Content = "";
            }
            else
            {
                string temp2 = "";//CheckWriteZero(PrGr.MySettings.EndFreqencyFWS);
                FreqFWSLabel.Content = Convert.ToString((Double)PrGr.MySettings.FrequencyFWS / 1000) + temp + _len.Text1 + "   -   " + Convert.ToString((Double)PrGr.MySettings.EndFreqencyFWS / 1000) + temp2 + _len.Text1;
                StepFWSLabel.Content = _len.Step + ": " + Convert.ToString(PrGr.MySettings.StepFWS) + " " + _len.Text2;
                TimeFWSLabel.Content = _len.Time + ": " + Convert.ToString( Convert.ToInt32((((PrGr.MySettings.EndFreqencyFWS - PrGr.MySettings.FrequencyFWS) / PrGr.MySettings.StepFWS) + 1) * PrGr.MySettings.TimeFWS) * 1000 )+ " / " + Convert.ToString(PrGr.MySettings.TimeFWS * 1000) + " "  + _len.Text4;
            }
            LaunchOneType();
        }

        private void CheckFUSS_Checked(object sender, RoutedEventArgs e)
        {
            {
                string temp1 = "";//CheckWriteZero(PrGr.MySettings.FrequencyFUSS);
                FreqFUSSLabel.Content = Convert.ToString((Double)PrGr.MySettings.FrequencyFUSS / 1000) + temp1 + _len.Text1 + "   -   ";
            }
            double temp = ((Double)PrGr.MySettings.FrequencyFUSS / 1000) + (Double)(PrGr.MySettings.AmountOfIterationFUSS * PrGr.MySettings.StepOfFreqencyFUSS) / 1000;
            {
                string strTemp = Convert.ToString(temp).Replace(',', _len.Text3[0]);
           
                if (temp * 1000 % 10 != 0)
                    FreqFUSSLabel.Content += strTemp + " " + _len.Text1;
                else if (temp * 1000 % 100 != 0)
                    FreqFUSSLabel.Content += strTemp + "0 " + _len.Text1;
                else if (temp * 1000 % 1000 != 0)
                    FreqFUSSLabel.Content += strTemp + "00 " + _len.Text1;
                else
                    FreqFUSSLabel.Content += temp + _len.Text3 + " " + _len.Text1;
            }
            StepFUSSLabel.Content = _len.Step + ": " + Convert.ToString(PrGr.MySettings.StepOfFreqencyFUSS) + " " + _len.Text2;
            TimeFUSSLabel.Content = _len.Time1 + ": " + Convert.ToString(PrGr.MySettings.Duration * 1000) + " " + _len.Text4;
            LaunchOneType();
        }

        private void CheckFW_Checked(object sender, RoutedEventArgs e)
        {                  
            FreqFWLabel.Content = PrGr.MySettings.RangeFW.Split('-')[0].Trim() + _len.Text3 + " " + _len.Text1 + "   -   " + PrGr.MySettings.RangeFW.Split('-')[1].Trim() + _len.Text3 + " " + _len.Text1;
            StepFWLabel.Content = _len.Step + ": " + Convert.ToString(PrGr.MySettings.StepFW) + " " + _len.Text1;
            TimeFWLabel.Content = _len.Time1 + ": " + Convert.ToString(PrGr.MySettings.Duration * 1000) + " " + _len.Text4;
            LaunchOneType();
        }

        private string CheckWriteZero(int frequence)
        {
            string temp = "";
            if (frequence % 10 != 0)
                temp = " ";
            else if (frequence % 100 != 0)
                temp = "0 ";
            else if (frequence % 1000 != 0)
                temp = "00 ";
            else
                temp = _len.Text3 + " ";
            return temp;
        }
    }
}

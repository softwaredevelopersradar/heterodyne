﻿using System.Windows;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private void ColExpPG_Checked_PGCollapsed(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions[2].Width = new GridLength(200, GridUnitType.Star);
            MainGrid.ColumnDefinitions[1].Width = new GridLength(4);
            PrGr.Visibility = Visibility.Visible;
            Line1.Visibility = Visibility.Visible;
        }

        private void ColExpPG_Unchecked_PGExpand(object sender, RoutedEventArgs e)
        {           
            PrGr.Visibility = Visibility.Collapsed;
            Line1.Visibility = Visibility.Collapsed;
            MainGrid.ColumnDefinitions[2].Width = new GridLength(0, GridUnitType.Star);
            MainGrid.ColumnDefinitions[1].Width = new GridLength(0);
        }

        private void ColExpLog_Unchecked_LogExpand(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions[6].Width = new GridLength(0,GridUnitType.Star);
            MainGrid.ColumnDefinitions[5].Width = new GridLength(0);
            Log.Visibility = Visibility.Collapsed;
            Line3.Visibility = Visibility.Collapsed;          
        }

        private void ColExpLog_Checked_LogCollapsed(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions[6].Width = new GridLength(132, GridUnitType.Star);
            MainGrid.ColumnDefinitions[5].Width = new GridLength(4);
            Log.Visibility = Visibility.Visible;
            Line3.Visibility = Visibility.Visible;
        }
    }
}

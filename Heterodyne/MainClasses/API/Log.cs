﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;


namespace Heterodyne 
{
  public partial class MainWindow : Window
    {
        private List<string> _txtRange;
        private List<string> _time;
        private List<Brush> _brushesList;
        private FlowDocument _myFlowDoc;
        private Paragraph _myParagraph;

        private void AddTextToLog(string text, int t, Brush LogBrush)
        {        
            Run HelloRun = new Run(DateTime.Now.ToLongTimeString());
            HelloRun.FontFamily = new FontFamily("Times New Roman");
            HelloRun.FontSize = 12 + t;
            HelloRun.Foreground = Brushes.Aquamarine;
            Run HelloMessage = new Run("  " + text + "\n");
            HelloMessage.FontFamily = new FontFamily("Times New Roman");
            HelloMessage.Foreground = LogBrush;
            HelloMessage.FontSize = 12 + t;
        
            _myParagraph.Inlines.Add(HelloRun);
            _myParagraph.Inlines.Add(HelloMessage);
            _myFlowDoc.PageWidth = Log.Width;
            _myFlowDoc.IsOptimalParagraphEnabled = true;
            _myFlowDoc.IsHyphenationEnabled = true;
            _myFlowDoc.Blocks.Add(_myParagraph);
            
            Log.Document = _myFlowDoc;

            _txtRange.Add(HelloMessage.Text);
            _time.Add(HelloRun.Text);
            _brushesList.Add(HelloMessage.Foreground);
            LogFontSize(t);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Log.Document.Blocks.Clear();
            _txtRange.Clear();
            _time.Clear();
            AddTextToLog("App loaded.\n", _koef, Brushes.LightGray);
        }

        private void LogFontSize(int p)
        {
            FlowDocument myFlowDoc = new FlowDocument();
            Paragraph myParagraph = new Paragraph();
       
            for (int i = 0; i < _time.Count; i++)
            {
                Run myRun11 = new Run(_time[i]);
                Run myRun12 = new Run(_txtRange[i]);
                myRun11.FontFamily = new FontFamily("Times New Roman");
                myRun11.FontSize = 12 + p;
                myRun11.Foreground = Brushes.Aquamarine;
                myRun12.FontFamily = new FontFamily("Times New Roman");
                myRun12.Foreground = _brushesList[i];
                myRun12.FontSize = 12 + p;
          
                myParagraph.Inlines.Add(myRun11);
                myParagraph.Inlines.Add(myRun12);
                myFlowDoc.PageWidth = Log.Width;
                myFlowDoc.IsOptimalParagraphEnabled = true;
                myFlowDoc.IsHyphenationEnabled = true;
                myFlowDoc.Blocks.Add(myParagraph);               
                Log.Document = myFlowDoc;
            }
        }
    }
}

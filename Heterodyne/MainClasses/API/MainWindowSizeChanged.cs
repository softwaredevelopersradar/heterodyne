﻿using System.Windows;

namespace Heterodyne
{
    partial class MainWindow : Window
    {
        private void MW_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            int n = 10;
            if ((MW.Height <= 412.6 && MW.Width <= 812.6) || MW.Height == MinHeight || MW.Height == MinHeight)
            {
                ChangeEllementSize(0);
            }          
            if(MW.WindowState == WindowState.Maximized)
            {
                ChangeEllementSize(n-2);
            }
            else
            {
                for (int i = 1; i < 2*n; i++)
                {
                    if (MW.Height > (400 + 12.5 * i) && MW.Width > (800 + 12.5 * i))
                    {
                        ChangeEllementSize(i / 2);
                    }
                }
            }        
        }
     
        private void ChangeEllementSize(int i)
        {
            PrGr.propertyGrid.FontSize = 12 + i * 0.5;
            error.FontSize = 12 + i * 1.5;
            CheckFUSS.FontSize = 13 + i * 1.5;
            Log.FontSize = 12 + i * 1.5;
            ErrorTextBox.FontSize = 12 + i * 1.5;
            HTRD.Height = 21 + 2 * i;
            StatusBarGrid1.Width = 240 + 15 * i;
            StatusBarGrid2.Width = 240 + 20 * i;
            StatusBarGrid1.Height = 23 + 3 * i;
            StatusBarGrid2.Height = 23 + 3 * i;
            FreqFWSLabel.FontSize = 14 + i * 1.5;
            StepFUSSLabel.FontSize = 12 + i * 1.5;
            Update.Height = 26 + i * 2;
            Upd.Width = 21 + i * 2;
            Log.LogFontSize(i);
            _koef = i;        
        }
    }
}

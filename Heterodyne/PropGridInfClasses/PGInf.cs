﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace Heterodyne.PropGridInfClasses
{
   public class PGInf
    {
        #region Parametrs
 
        private string lenguageCommon;
        private int speedCommon;
        private string comPortCommon;
     


       public PGInf() { 
            lenguageCommon = "RU";
            comPortCommon = "COM3";
            speedCommon = 2400;
        }
        #endregion

        #region Common
        [PropertyOrder(0)]
       
        [DisplayName("Температура")]
        public string LenguageCommon
        {
            get { return lenguageCommon; }
            set
            {
                if (lenguageCommon == value)
                    return;
                lenguageCommon = value;
                OnPropertyChanged();
            }
        }



        [PropertyOrder(1)]
       
        [DisplayName("Влажность")]   
        public int SpeedCommon
        {
            get { return speedCommon; }
            set
            {
                if (speedCommon == value)
                    return;
                if (speedCommon < 0)
                    return;
                speedCommon = value;
                OnPropertyChanged();
            }
        }

        [PropertyOrder(2)]        
        [DisplayName("Заряд АКБ")]      
        public string ComPortCommon
        {
            get { return comPortCommon; }
            set
            {
                if (comPortCommon == value)
                    return;
                comPortCommon = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public PGInf Clone()
        {
            return new PGInf()
            {
                LenguageCommon = lenguageCommon,
                SpeedCommon = speedCommon,
                ComPortCommon = comPortCommon,
               
            };
        }

        public void Update(PGInf data)
        {
            LenguageCommon = data.LenguageCommon;
            SpeedCommon = data.SpeedCommon;
            ComPortCommon = data.ComPortCommon;           
        }

        public bool Compare(PGInf data)
        {
            return lenguageCommon != data.LenguageCommon || speedCommon != data.SpeedCommon || comPortCommon != data.ComPortCommon
                ? false
                : true;
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace Heterodyne.PropGridInfClasses
{
    class Editor1 : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(PGInf.ComPortCommon), "Temperatures" },
            { nameof(PGInf.LenguageCommon), "Humiditis" },
            { nameof(PGInf.SpeedCommon), "Charges" },
        };

        public Editor1(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("dictionary12.xaml", UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }

    }
}

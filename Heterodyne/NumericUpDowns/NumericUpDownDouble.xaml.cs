﻿using Heterodyne.PropGridClasses;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для NumericUpDown.xaml
    /// </summary>
    public partial class NumericUpDownDouble : UserControl
    {
        public double Increment { get; set; } = 1000;
        public double Minimum { get; set; } = 1000;
        public double Maximum { get; set; } = 10000000;

        public NumericUpDownDouble()
        {
            InitializeComponent();
        }


        private void ButtonUp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double value = double.Parse(textPathData.Text.Replace(',', '.'), System.Globalization.CultureInfo.InvariantCulture);
                textPathData.Text = Convert.ToString(value + Increment);
                if (value >= Maximum)
                {
                    textPathData.Text = Convert.ToString(Maximum);
                    return;
                }
                BindingExpression expression = textPathData.GetBindingExpression(TextBox.TextProperty);
                if (expression != null)
                {
                    expression.UpdateSource();
                    expression.UpdateTarget();
                }
            }
            catch (Exception ex)
            {
                textPathData.Text = Convert.ToString(Minimum);
            }
        }

        private void ButtonDown_Click(object sender, RoutedEventArgs e)
        {  
            try {

                double value = double.Parse(textPathData.Text.Replace(',', '.'), System.Globalization.CultureInfo.InvariantCulture);

                if (value <= Minimum - 1d)
                {
                    textPathData.Text = Convert.ToString(0);
                    return;
                }
                textPathData.Text = Convert.ToString(value - Increment);
                BindingExpression expression = textPathData.GetBindingExpression(TextBox.TextProperty);
                if (expression != null)
                {
                    expression.UpdateSource();
                    expression.UpdateTarget();
                }
            }
            catch (Exception)
            {
                textPathData.Text = Convert.ToString(Minimum);
            }
        }

        private void Numeric_Up_Down_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}

﻿using Heterodyne.PropGridClasses;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для NumericUpDownx2.xaml
    /// </summary>
    public partial class NumericUpDownx2 : UserControl
    {
        private static NumericUpDownx2 ins;
        public static double[,] MassRange;
        private double _offsetKoef=0;
        public static double OffsetKoef{ get { return ins._offsetKoef; } set {
                ins._offsetKoef = value;
               ins.textPathData.Text = Convert.ToString(MassRange[ins.i, 0] - ins._offsetKoef) + " - " + Convert.ToString(MassRange[ins.i, 1] - ins._offsetKoef);              
            } }

        public NumericUpDownx2()
        { 
            InitializeComponent();
            ins = this;
            textPathData1.Text = Convert.ToString(PG.ini.IniReadValue("Parameters", "lineFW"));
            i = Convert.ToInt32(textPathData1.Text);           
        }
       
        private int i;

        private void ButtonUp_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if (i >= MassRange.Length/2 - 2)
                return;
                i++;
                textPathData.Text = Convert.ToString(MassRange[i, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[i, 1] - _offsetKoef);
                textPathData1.Text = Convert.ToString(i);
            }
            catch (IndexOutOfRangeException)
            {
                textPathData1.Text = Convert.ToString(MassRange.Length - 1);
                textPathData.Text = Convert.ToString(MassRange[MassRange.Length/2-2, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[MassRange.Length/2-1, 1] - _offsetKoef);
            }
           PG.ini.IniWriteValue("Parameters", "lineFW", Convert.ToString(i));
        }

        private void ButtonDown_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if (i <= 1)
                return;
            i--;
                textPathData.Text = Convert.ToString(MassRange[i, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[i, 1] * - _offsetKoef);
                textPathData1.Text = Convert.ToString(i);
            }
            catch (IndexOutOfRangeException)
            {
                textPathData1.Text = Convert.ToString(0);
                textPathData.Text = Convert.ToString(MassRange[0, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[0, 1] - _offsetKoef);
            }
            PG.ini.IniWriteValue("Parameters", "lineFW", Convert.ToString(i));
        }

        private void ButtonDown1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (i <= 1)
                {
                    return;
                }
                textPathData1.Text = Convert.ToString(Convert.ToInt32(textPathData1.Text) - 1);
                i = Convert.ToInt32(textPathData1.Text);
                textPathData.Text = Convert.ToString(MassRange[i, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[i, 1] - _offsetKoef);
            }
            catch (IndexOutOfRangeException)
            {
                textPathData1.Text = Convert.ToString(0);
                textPathData.Text = Convert.ToString(MassRange[0, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[0, 1] - _offsetKoef);
            }
      
            PG.ini.IniWriteValue("Parameters", "lineFW", Convert.ToString(i));
        }

        private void ButtonUp1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (i >= MassRange.Length/2 - 2)
                    return;
                textPathData1.Text = Convert.ToString(Convert.ToInt32(textPathData1.Text) + 1);
                i = Convert.ToInt32(textPathData1.Text);
                textPathData.Text = Convert.ToString(MassRange[i, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[i, 1] - _offsetKoef);
            }
            catch (IndexOutOfRangeException)
            {
                textPathData1.Text = Convert.ToString(MassRange.Length/2 - 1);
                textPathData.Text = Convert.ToString(MassRange[MassRange.Length/2 - 1, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[MassRange.Length/2 - 1, 1] - _offsetKoef);
            }
            PG.ini.IniWriteValue("Parameters", "lineFW", Convert.ToString(i));
        }

        private void TextPathData1_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                textPathData.Text = Convert.ToString(MassRange[Convert.ToInt32(textPathData1.Text), 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[Convert.ToInt32(textPathData1.Text), 1] - _offsetKoef);
                i = Convert.ToInt32(textPathData1.Text);
            }
            catch(IndexOutOfRangeException)
            {
                textPathData1.Text = Convert.ToString(MassRange.Length/2 - 2);
                textPathData.Text = Convert.ToString(MassRange[MassRange.Length/2-2, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[MassRange.Length/2-2, 1] - _offsetKoef);
            }
            catch (Exception)
            {
                textPathData1.Text = Convert.ToString(0);
                textPathData.Text = Convert.ToString(MassRange[0, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[0, 1] - _offsetKoef);
            }
            PG.ini.IniWriteValue("Parameters", "lineFW", Convert.ToString(Convert.ToInt32(textPathData1.Text)));
        }

        private void TextPathData1_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {            
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ButtonDown1_Loaded(object sender, RoutedEventArgs e)
        {
            if (Convert.ToBoolean(PG.ini.IniReadValue("Parameters", "offset")) == true)
                _offsetKoef = 0.5;
            else _offsetKoef = 0;
            i = Convert.ToInt32(textPathData1.Text);
            textPathData.Text = Convert.ToString(MassRange[i, 0] - _offsetKoef) + " - " + Convert.ToString(MassRange[i, 1] - _offsetKoef);
        }
    }
}


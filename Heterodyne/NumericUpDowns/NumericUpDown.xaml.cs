﻿using Heterodyne.PropGridClasses;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Heterodyne
{
    /// <summary>
    /// Логика взаимодействия для NumericUpDown.xaml
    /// </summary>
    public partial class NumericUpDown : UserControl
    {
        public int Increment { get; set; } = 1;
        public int Maximum { get; set; } = 1000000;
        public string Mode { get; set; } = "Normal";

        private int[] _massNumFreqFUSS = { 2, 4, 8, 16, 32, 64, 121 };
        private int[] _massStepFreqFW = { 1, 3, 5, 8, 15};
        private int i;

        public static string[] massiveRangeFW;

      
        public NumericUpDown()
        {
            InitializeComponent();
        }

        private void ButtonUp_Click(object sender, RoutedEventArgs e)
        {
            switch (Mode)
            {
                case "Normal":
                    try
                    {      
                        if (Convert.ToInt32(textPathData.Text) >= Maximum)
                            return;
                        textPathData.Text = Convert.ToString(Convert.ToInt32(textPathData.Text) + Increment);
                       
                    }
                    catch(Exception)
                    {
                        textPathData.Text = Convert.ToString(Increment);
                    }
                    break;
                case "NumFreqFUSS":
                    try
                    {
                        if (i >= _massNumFreqFUSS.Length)
                            return;
                        i++;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i - 1]);
                    }
                    catch (Exception)
                    {
                        i = 1;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i - 1]);
                    }
                    break;
                case "StepFreqFW":
                    try
                    {
                        if (i >= _massStepFreqFW.Length)
                            return;
                        i++;
                        textPathData.Text = Convert.ToString(_massStepFreqFW[i - 1]);
                        break;
                    }                   
                    catch (Exception)
                    {
                        i = 1;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i - 1]);                     
                    }
                    break;         
            }
        }

        private void ButtonDown_Click(object sender, RoutedEventArgs e)
        {
            switch (Mode)
            {
                case "Normal":
                    try {
                        if (Convert.ToInt32(textPathData.Text) <= Increment - 1)
                        {
                            textPathData.Text = Convert.ToString(0);
                            return; }
                    textPathData.Text = Convert.ToString(Convert.ToInt32(textPathData.Text) - Increment);
                    }
                    catch (Exception)
                    {
                        textPathData.Text = Convert.ToString(Increment);
                    }
                    break;
                case "NumFreqFUSS":
                    try
                    {
                        if (i <= 1)
                            return;
                        i--;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i - 1]);
                    }
                    catch (Exception)
                    {
                        i = 1;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i-1]);
                       
                    }
            break;
                case "StepFreqFW":
                    try
                    {
                        if (i <= 1)
                            return;
                        i--;
                        textPathData.Text = Convert.ToString(_massStepFreqFW[i - 1]);
                    }
                    catch (Exception)
                    {
                        i = 1;
                        textPathData.Text = Convert.ToString(_massNumFreqFUSS[i - 1]);
                    }
                    break;              
            }
        }

        private void Numeric_Up_Down_Loaded(object sender, RoutedEventArgs e)
        {
            switch (Mode)
            {
                case "Normal":
                    break;
                case "NumFreqFUSS":
                    i = Array.IndexOf(_massNumFreqFUSS, Convert.ToInt32(textPathData.Text));
                    break;
                case "StepFreqFW":
                    i = Array.IndexOf(_massStepFreqFW, Convert.ToInt32(textPathData.Text));
                    break;
                case "RangeFW":
                    i = Array.IndexOf(massiveRangeFW, textPathData.Text);
                    break;
            }
        }

        private void TextPathData_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void TextPathData_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(textPathData.Text) < 0)
                    textPathData.Text = Convert.ToString(0);
            }
            catch { }
        }
    }
}

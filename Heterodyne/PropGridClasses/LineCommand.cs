﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Heterodyne.PropGridClasses
{
    class LineCommand
    {
        #region Parameters
        public static RelayCommand ExecuteCommandLineP;
        public static RelayCommand ExecuteCommandLineM;

        public static RelayCommand IterationLineP
        {
            get
            {
                return ExecuteCommandLineP ??
                    (ExecuteCommandLineP = new RelayCommand(IterationLineP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandLineP == value) return;
                ExecuteCommandLineP = value;
            }
        }
        public static RelayCommand IterationLineM
        {
            get
            {
                return ExecuteCommandLineM ??
                    (ExecuteCommandLineM = new RelayCommand(IterationLineM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandLineM == value) return;
                ExecuteCommandLineM = value;
            }
        }
        public static string[] Mass { get; set; }
        public static string Item { get; set; }
        #endregion


        #region Incrementation
        private static void IterationLineP_Executed(object e)
        {

            if (Convert.ToInt32((e as TextBox).Text) >= Mass.Length)
                return;
            try
            {
                    Item = Mass[Convert.ToInt32((e as TextBox).Text)];
                    (e as TextBox).Text = Convert.ToString(Convert.ToInt32((e as TextBox).Text) + 1);
            }
            catch(IndexOutOfRangeException)
            {
                Item = "Value is too big";
            }
        }

        private static void IterationLineM_Executed(object e)
        {
            if (Convert.ToInt32((e as TextBox).Text) <= 0)
                return;
            else
            {
                (e as TextBox).Text = Convert.ToString(Convert.ToInt32((e as TextBox).Text) - 1);
                if (Convert.ToInt32((e as TextBox).Text) > 0)
                {
                  Item = Mass[Convert.ToInt32((e as TextBox).Text) - 1];
                }else Item = "0 - 0";
            }
        }
        #endregion

        public static bool CanExecute(object e)
        {
            return true;
        }

      
        public static void Update(int Line)
        {
            Item = Mass[Line];
        }

    }
}

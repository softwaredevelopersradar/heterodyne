﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace Heterodyne.PropGridClasses
{
    class ValueCorrectEditor : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "FrequencyFWSs"},
            { nameof(PG.TimeFWS), "TimeFWSs"},
            { nameof(PG.StepFWS), "StepFWSs" },

            { nameof(PG.AmountOfSignalPerSecFUSS), "AmountOfSignalPerSecFUSSs"},
            { nameof(PG.FrequencyFUSS), "FrequencyFUSSs"},
            { nameof(PG.AmountOfIterationFUSS), "AmountOfIterationFUSSs" },

            { nameof(PG.StepFW), "StepFWs" },
             { nameof(PG.Duration), "Durations" },
              { nameof(PG.EndFreqencyFWS), "EndFreqencyFWSs" },
        };

        public ValueCorrectEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/Heterodyne;component/xamlResources/dictionary11.xaml", UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }

    }
}

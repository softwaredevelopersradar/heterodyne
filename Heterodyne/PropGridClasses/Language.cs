﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heterodyne.PropGridClasses
{
    class Language : ObservableCollection<string>
    {
        public Language()
        {
            Add("EN");
            Add("RU");
        }
    }
}

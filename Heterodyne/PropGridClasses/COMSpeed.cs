﻿using System.Collections.ObjectModel;

namespace Heterodyne.PropGridClasses
{
    class COMSpeed : ObservableCollection<int>
    {
        public COMSpeed()
        {
            Add(2400);
            Add(4800);
            Add(9600);
            Add(19200);
            Add(38400);
            Add(57600);
            Add(115200);
        }
    }
}
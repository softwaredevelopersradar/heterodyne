﻿using Heterodyne.PropGridClasses;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace Heterodyne
{
    class Editor : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "FrequencyFWSs"},
            { nameof(PG.TimeFWS), "TimeFWSs"},
            { nameof(PG.AutoFWS), "AutoFWSs"},
            { nameof(PG.StepFWS), "StepFWSs"},
            { nameof(PG.EndFreqencyFWS), "EndFreqencyFWSs"},

            { nameof(PG.AmountOfSignalPerSecFUSS), "AmountOfSignalPerSecFUSSs"},
            { nameof(PG.StepOfFreqencyFUSS), "NumberOfFreqencyFUSSs"},
            { nameof(PG.FrequencyFUSS), "FrequencyFUSSs"},
            { nameof(PG.AmountOfIterationFUSS), "AmountOfIterationFUSSs"},

            { nameof(PG.ComPortCommon), "ComPortCommons"},
            { nameof(PG.Language), "LenguageCommons"},
            { nameof(PG.SpeedCommon), "SpeedCommons"},

            { nameof(PG.StepFW), "StepFWs"},
            { nameof(PG.RangeFW), "RangeFWs"},      
            { nameof(PG.Offset), "Offsets"},
            { nameof(PG.Duration), "Durations"},
            { nameof(PG.VersionType), "VersionType"}
        };

        public Editor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
       
            var resource = new ResourceDictionary
            {
                Source = new Uri("/Heterodyne;component/PropGridClasses/Resources/Template.xaml", UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }       
    }
}
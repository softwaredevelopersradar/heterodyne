﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Heterodyne.PropGridClasses
{
  public  class IterationCommand
    {
        #region Parameters
        public static RelayCommand ExecuteCommandP;
        public static RelayCommand ExecuteCommandM;

        public static RelayCommand IterationP
        {
            get
            {
                return ExecuteCommandP ??
                    (ExecuteCommandP = new RelayCommand(IterationP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandP == value) return;
                ExecuteCommandP = value;
            }
        }
        public static RelayCommand IterationM
        {
            get
            {
                return ExecuteCommandM ??
                    (ExecuteCommandM = new RelayCommand(IterationM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandM == value) return;
                ExecuteCommandM = value;
            }
        }

        #endregion

        #region Incrementation
        private static void IterationP_Executed(object e)
        {
            if ((e as TextBox).IsFocused == false)
                (e as TextBox).Focus();
            (e as TextBox).Text = Convert.ToString(Convert.ToInt32((e as TextBox).Text) + 1);
        }

        private static void IterationM_Executed(object e)
        {
            if ((e as TextBox).IsFocused == false)
                (e as TextBox).Focus();
            if (Convert.ToInt32((e as TextBox).Text) <= 0)
                return;
            (e as TextBox).Text = Convert.ToString(Convert.ToInt32((e as TextBox).Text) - 1);
        }
        #endregion

        public static bool CanExecute(object e)
        {
            return true;
        }   
  }
}

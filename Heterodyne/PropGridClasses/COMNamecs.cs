﻿using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Collections.ObjectModel;

namespace Heterodyne.PropGridClasses
{
    class COMNamecs : ObservableCollection<string>
    {
        public COMNamecs()
        {
            UpdatePort();
        }

        public void UpdatePort()
        {
            List<string> portNames = SerialPort.GetPortNames().ToList();
            if (portNames.Count == 0)
            {
                portNames.Add("COM1");
                portNames.Add("COM2");
            }

            if (this.Equals(portNames))
                return;
            var except = portNames.Except(this).ToList();
            foreach (var name in except)
                Add(name);
            except = this.Except(portNames).ToList();
            foreach (var name in except)
               Remove(name);
        }
    }
}

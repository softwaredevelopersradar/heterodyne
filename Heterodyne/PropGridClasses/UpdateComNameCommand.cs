﻿using System.Windows.Controls;

namespace Heterodyne.PropGridClasses
{
    class UpdateComNameCommand
    {

        public static RelayCommand UpdateCommandP;

        public static RelayCommand Update
        {
            get
            {
                return UpdateCommandP ??
                    (UpdateCommandP = new RelayCommand(Update_Executed, CanExecute));
            }
            set
            {
                if (UpdateCommandP == value) return;
                UpdateCommandP = value;
            }
        }
     
        private static void Update_Executed(object e)
        {
            (e as ComboBox).ItemsSource = new COMNamecs();
        }
 
        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}

﻿using System;
using System.Windows.Controls;


namespace Heterodyne.PropGridClasses
{
    class RangeCommand
    {

        #region Parameters
        public static RelayCommand ExecuteCommandRangeP;
        public static RelayCommand ExecuteCommandRangeM;

        public static RelayCommand IterationRangeP
        {
            get
            {
                return ExecuteCommandRangeP ??
                    (ExecuteCommandRangeP = new RelayCommand(IterationLineP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandRangeP == value) return;
                ExecuteCommandRangeP = value;
            }
        }
        public static RelayCommand IterationRangeM
        {
            get
            {
                return ExecuteCommandRangeM ??
                    (ExecuteCommandRangeM = new RelayCommand(IterationLineM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandRangeM == value) return;
                ExecuteCommandRangeM = value;
            }
        }
        public static string[] mass;
        public static int i=1;    
        #endregion


        #region Incrementation
        private static void IterationLineP_Executed(object e)
        {
            i++;
         
            if (i > mass.Length)
                return;
            try
            {
                (e as TextBox).Text = (String)mass[i];              
            }
            catch (IndexOutOfRangeException)
            {
                (e as TextBox).Text = "Слишком большое число";
            }
            catch (NullReferenceException)
            {
                (e as TextBox).Text = "Проверьте файл с линиями";
            }
        }

        private static void IterationLineM_Executed(object e)
        {
            if ((e as TextBox).Text == (String)"0 - 0")
                return;
            i--;
            if (i == 0)
            { (e as TextBox).Text = (String)"0 - 0";
                return; }
            (e as TextBox).Text = (String)mass[i];
        }
        #endregion

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}

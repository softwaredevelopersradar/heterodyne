﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Heterodyne.PropGridClasses
{
    class ChangeFrequency
    {
        #region Parameters
        public static RelayCommand ExecuteCommandFWSP;
        public static RelayCommand ExecuteCommandFWSM;
        public static RelayCommand ExecuteCommandFUSSP;
        public static RelayCommand ExecuteCommandFUSSM;

        public static RelayCommand CHFrequencyFWSP
        {
            get
            {
                return ExecuteCommandFWSP ??
                    (ExecuteCommandFWSP = new RelayCommand(CHFrequencyP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandFWSP == value) return;
                ExecuteCommandFWSP = value;
            }
        }

        public static RelayCommand CHFrequencyFWSM
        {
            get
            {
                return ExecuteCommandFWSM ??
                    (ExecuteCommandFWSM = new RelayCommand(CHFrequencyM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandFWSM == value) return;
                ExecuteCommandFWSM = value;
            }
        }

        public static RelayCommand CHFrequencyFUSSP
        {
            get
            {
                return ExecuteCommandFUSSP ??
                    (ExecuteCommandFUSSP = new RelayCommand(CHFrequencyP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandFUSSP == value) return;
                ExecuteCommandFUSSP = value;
            }
        }
        public static RelayCommand CHFrequencyFUSSM
        {
            get
            {
                return ExecuteCommandFUSSM ??
                    (ExecuteCommandFUSSM = new RelayCommand(CHFrequencyM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandFUSSM == value) return;
                ExecuteCommandFUSSM = value;
            }
        }
        #endregion

        #region  increase
        private static void CHFrequencyP_Executed(object e)
        {
            if (Convert.ToDouble((e as TextBox).Text) >= 100000)
            {
                return;
            }
            else
            (e as TextBox).Text = Convert.ToString(Convert.ToDouble((e as TextBox).Text) + 1000);
            // var param = (Tuple<object, object>)e;
            // TextBox txt1 = (TextBox)param.Item1;
            // TextBox txt2 = (TextBox)param.Item2;

            // txt1.Text = Convert.ToString(Convert.ToInt32(txt1.Text) + Convert.ToInt32(txt2.Text));
            //Convert.ToString(Convert.ToInt32(param.Item2) + Convert.ToInt32(param.Item1));
            // a  = a + b;
        }
      

        private static void CHFrequencyM_Executed(object e)
        {
            if (Convert.ToInt32((e as TextBox).Text.Trim()) <= 1)
            { (e as TextBox).Text = "0"; }
            else
            {(e as TextBox).Text = Convert.ToString(Convert.ToDouble((e as TextBox).Text) - 1000);}
            // Convert.ToString(Convert.ToInt32(param.Item2) - Convert.ToInt32(param.Item1));
        }

        #endregion

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}

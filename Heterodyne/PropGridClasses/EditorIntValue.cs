﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Input;

namespace Heterodyne.PropGridClasses
{
   public static class EditorIntValue 
    {
            #region TextBox Correct FreqKHz
            public static readonly DependencyProperty TextBoxDoubleFreqKHzChanged =
                DependencyProperty.RegisterAttached("Duration", typeof(bool), typeof(EditorIntValue),
                new FrameworkPropertyMetadata(false, OnTextBoxDoubleFreqKHzChanged));

        private static void OnTextBoxDoubleFreqKHzChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound)
            {
                textBox.PreviewTextInput -= TextBoxDoubleFreqKHzPreviewInput;
            }
            if (needToBind)
            {
                textBox.PreviewTextInput += TextBoxDoubleFreqKHzPreviewInput;
            }
        }

        private static void TextBoxDoubleFreqKHzPreviewInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                char[] tbText = (sender as TextBox).Text.ToCharArray();

                List<string> s = new List<string>();
                for (int i = 0; i < tbText.Length; i++)
                {
                    s.Add(tbText[i].ToString());
                }

                if (e.Text == "," || e.Text == ".")
                {
                    if ((s.IndexOf(",") != -1) || s.IndexOf(".") != -1)
                    {
                        e.Handled = true;
                    }
                    // ,
                    return;
                }

                if ((Convert.ToChar(e.Text) >= '0') && (Convert.ToChar(e.Text) <= '9'))
                {
                    // цифра
                    return;
                }

                // остальные символы запрещены
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxDoubleFreqKHzPreviewInput(TextBox target, bool value)
        {
            target.SetValue(TextBoxDoubleFreqKHzChanged, value);
        }

        public static bool GetTextBoxDoublePreviewInput(TextBox target)
        {
            return (bool)target.GetValue(TextBoxDoubleFreqKHzChanged);
        }
        #endregion


        #region TextBox Correct Double Value
        public static readonly DependencyProperty TextBoxDoubleValuesChanged =
                DependencyProperty.RegisterAttached("TimeFWSs", typeof(bool), typeof(EditorIntValue),
                new FrameworkPropertyMetadata(false, OnTextBoxDoubleValuesChanged));

            private static void OnTextBoxDoubleValuesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                var textBox = sender as TextBox;
                if (textBox == null) return;

                var wasBound = (bool)(e.OldValue);
                var needToBind = (bool)(e.NewValue);
                if (wasBound)
                {
                    textBox.PreviewTextInput += TextBoxDoubleValuesPreviewInput;
                    textBox.KeyUp -= TextBoxDoubleValuesKeyUp;
                }
                if (needToBind)
                {
                    textBox.PreviewTextInput += TextBoxDoubleValuesPreviewInput;
                    textBox.KeyUp += TextBoxDoubleValuesKeyUp;
                }
            }

            private static void TextBoxDoubleValuesPreviewInput(object sender, TextCompositionEventArgs e)
            {
                try
                {
                    char[] tbText = (sender as TextBox).Text.ToCharArray();

                    List<string> s = new List<string>();
                    for (int i = 0; i < tbText.Length; i++)
                    {
                        s.Add(tbText[i].ToString());
                    }
                  
                    if ((Convert.ToChar(e.Text) >= '0') && (Convert.ToChar(e.Text) <= '9'))
                    {
                        // цифра
                        return;
                    }
                    // остальные символы запрещены
                    e.Handled = true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            private static void TextBoxDoubleValuesKeyUp(object sender, KeyEventArgs e)
            {
                try
                {
                    var textbox = sender as TextBox;
                    if (textbox == null) return;
                    if ( e.Key == Key.Back || e.Key == Key.Escape) //e.Key==Key.OemComma||
                        return;

                    var eS = textbox.Text[textbox.Text.Length - 1];
                    if (eS.ToString() == "," || eS.ToString() == ".")
                    {
                        string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);                
                        textbox.SelectionStart = textbox.Text.Length;
                        textbox.SelectionLength = 0;                    
                    }

                    BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
                    if (expression != null) expression.UpdateSource();
                    e.Handled = true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            public static void SetTextBoxDoubleValues(TextBox target, bool value)
            {
                target.SetValue(TextBoxDoubleValuesChanged, value);
            }

            public static bool GetTextBoxDoubleValues(TextBox target)
            {
                return (bool)target.GetValue(TextBoxDoubleValuesChanged);
            }

            #endregion

            #region CommitOnTyping
            public static readonly DependencyProperty CommitOnTypingProperty =
                DependencyProperty.RegisterAttached("StepFWSs", typeof(bool), typeof(EditorIntValue),
                new FrameworkPropertyMetadata(false, OnCommitOnTypingChanged));

            private static void OnCommitOnTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                var textbox = sender as TextBox;
                if (textbox == null) return;

                var wasBound = (bool)(e.OldValue);
                var needToBind = (bool)(e.NewValue);

                if (wasBound)
                    textbox.KeyUp -= TextBoxCommitValueWhileTyping;

                if (needToBind)
                    textbox.KeyUp += TextBoxCommitValueWhileTyping;
            }

            static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
            {
                if (e.Key == Key.Escape || e.Key == Key.OemPeriod || e.Key == Key.OemComma || e.Key == Key.Decimal)
                    return;

                var textbox = sender as TextBox;
                if (textbox == null) return;
                BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
                if (expression != null)
                {
                    expression.UpdateSource();
                }
                e.Handled = true;
            }

            public static void SetCommitOnTyping(TextBox target, bool value)
            {
                target.SetValue(CommitOnTypingProperty, value);
            }

            public static bool GetCommitOnTyping(TextBox target)
            {
                return (bool)target.GetValue(CommitOnTypingProperty);
            }
            #endregion
   }
}

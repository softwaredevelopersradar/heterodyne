﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Heterodyne.PropGridClasses
{
    class NumFreqFUSSCommand
    {
        #region Parameters
        public static RelayCommand ExecuteCommandP;
        public static RelayCommand ExecuteCommandM;

        public static RelayCommand IterationP
        {
            get
            {
                return ExecuteCommandP ??
                    (ExecuteCommandP = new RelayCommand(IterationP_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandP == value) return;
                ExecuteCommandP = value;
            }
        }
        public static RelayCommand IterationM
        {
            get
            {
                return ExecuteCommandM ??
                    (ExecuteCommandM = new RelayCommand(IterationM_Executed, CanExecute));
            }
            set
            {
                if (ExecuteCommandM == value) return;
                ExecuteCommandM = value;
            }
        }

        #endregion

        private static int[] _mass = {2, 4, 8, 16, 32, 64, 121};
        private static int _i = 0;

        #region Incrementation
        private static void IterationP_Executed(object e)
        {
           
            if (_i >= _mass.Length)
                return;
            _i++;
            (e as TextBox).Text = Convert.ToString(_mass[_i-1]);
        }

        private static void IterationM_Executed(object e)
        {
           
            if (_i <= 1)
                return;
            _i--;
            (e as TextBox).Text = Convert.ToString(_mass[_i-1]);
        }
        #endregion

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}

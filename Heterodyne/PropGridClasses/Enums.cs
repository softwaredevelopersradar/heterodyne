﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heterodyne.PropGridClasses
{
    public enum Version : byte
    {
        [Description("Main")]
        Main,
        [Description("Kvetka")]
        Kvetka
    }
}

﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls.WpfPropertyGrid;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using CNT;

namespace Heterodyne.PropGridClasses
{
    [Serializable]
    [RefreshProperties(RefreshProperties.All)]
    [BrowsableCategory(true)] 
    [DesignerCategory()]
    [CategoryOrder("Common",0)]
    [CategoryOrder("FWS", 1)]
    [CategoryOrder("FHSS", 2)]
    [CategoryOrder("Grid", 3)]

    public class PG : DependencyObject, INotifyPropertyChanged, IModelMethods<PG>
    {
        const string Common = "Common";
        const string FWS = "FWS";
        const string FHSS = "FHSS";
        const string Grid = "Grid";

        public void InitLineFromDocument()
        {          
            double[,] MassRangeFW = new double[Mass.Length, 2];
            string[] selectStrip;
            string[] mass;
            try
            {               
                string textFromFile;
                FileStream fstream = File.OpenRead(Environment.CurrentDirectory + @"\textResources\NumberStrip.txt");

                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                textFromFile = System.Text.Encoding.Default.GetString(array);

                mass = textFromFile.Split('\n');
               
                for (int i = 0; i < mass.Length - 1; i++)
                {
                    selectStrip = mass[i].Split('-');
                    selectStrip[0] = selectStrip[0].Trim();
                    selectStrip[1] = selectStrip[1].Trim();

                    mass[i] = selectStrip[0] + " - " + selectStrip[1];
                    MassRangeFW[i, 0] = Int32.Parse(selectStrip[0]);
                    MassRangeFW[i, 1] = Int32.Parse(selectStrip[1]);
                }
                PG.Mass = mass;
                NumericUpDownx2.MassRange = MassRangeFW;
                fstream.Close();
               
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File \"NumberStrip.txt\" not found");
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Something happened with file \"NumberStrip.txt\"" +"\n" + ex.Message);
                
            }
        }

        #region Parametrs
        private double _frequencyFWS;
        private double _endFreqencyFWS;
        private double _stepFWS = 1;
        private double _frequencyFUSS;
        private int _amountOfIterationFUSS;
        private int _stepOfFreqencyFUSS;
        private int _amountOfSignalPerSecFUSS;
        private int _lineFW;
        private int _stepFW;
        private bool _offset;
        private int _timeFWS;
        private bool _autoFWS;
        private int _speedCommon;
        private string _comPortCommon;
        private string _rangeFW;
        private int _duration =10;
        public string _language;
        private Version _versionType;


        public static string[] Mass = new string[202];
        public static IniFile ini;


        public PG() //начальные значения берем из файла SettingsHTRD.ini
        {
            InitLineFromDocument();
            ini = new IniFile(Environment.CurrentDirectory + @"\textResources\SettingsHTRD.ini");//MainWindow.path + @"\textResources\SettingsHTRD.ini");           
            try
            {
                _timeFWS = Convert.ToInt32(ini.IniReadValue("Parameters", "timeFWS"));
                _autoFWS = Convert.ToBoolean(ini.IniReadValue("Parameters", "auto"));
                _stepFWS = Convert.ToDouble(ini.IniReadValue("Parameters", "stepFWS"));
                _frequencyFUSS = Convert.ToInt32(ini.IniReadValue("Parameters", "frequencyFUSS"));
                _frequencyFWS = Convert.ToDouble(ini.IniReadValue("Parameters", "FrequencyFWS"));
                _endFreqencyFWS = Convert.ToInt32(ini.IniReadValue("Parameters", "endFreqencyFWS"));
                _amountOfSignalPerSecFUSS = Convert.ToInt32(ini.IniReadValue("Parameters", "amountOfSignalPerSecFUSS"));
                _amountOfIterationFUSS = Convert.ToInt32(ini.IniReadValue("Parameters", "amountOfIterationFUSS"));
                _stepOfFreqencyFUSS = Convert.ToInt32(ini.IniReadValue("Parameters", "stepOfFreqencyFUSS"));
                _comPortCommon = ini.IniReadValue("COM", "Name");
                _speedCommon = Convert.ToInt32(ini.IniReadValue("COM", "Speed"));
                _lineFW = Convert.ToInt32(ini.IniReadValue("Parameters", "lineFW"));
                _stepFW = Convert.ToInt32(ini.IniReadValue("Parameters", "stepFW"));
                _rangeFW = Mass[_lineFW];
                _offset = Convert.ToBoolean(ini.IniReadValue("Parameters", "offset"));
                _duration = Convert.ToInt32(ini.IniReadValue("Parameters", "duration"));

                VersionType = Convert.ToString(ini.IniReadValue("Parameters", "versionType")) == "Main" ? Version.Main : Version.Kvetka;
                
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Check ini file \"SettingsHTRD\" " + Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + @"\textResources\SettingsHTRD.ini\n" + ex.Message);
                _timeFWS = 1;
                _autoFWS = true;
                _stepFWS = 1000;
                _frequencyFUSS = 30000;
                _frequencyFWS = 30000;
                _endFreqencyFWS = 35000;
                _amountOfSignalPerSecFUSS = 100;
                _amountOfIterationFUSS = 16;
                _stepOfFreqencyFUSS = 25;
                _comPortCommon = "COM2";
                _speedCommon = 5760;
                _lineFW = 1;
                _stepFW = 1;
                _rangeFW = "255 - 265";
                _offset = false;
                _duration = 10;

                _versionType = Version.Main;
            }
        }
        #endregion
     
        #region Common
        // Speed Of COM Port     
        [PropertyOrder(3)]       
        [CategoryAttribute(Common)]        
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        public int SpeedCommon 
        {
            get { return _speedCommon; }
            set
            {
                if (_speedCommon == value)
                    return;
                if (_speedCommon < 0)
                    return;
                _speedCommon = value;
                ini.IniWriteValue("COM", "Speed",Convert.ToString( _speedCommon));
                OnPropertyChanged();
            } 
        }

        //Name Of COM Port
        [PropertyOrder(2)]
        [CategoryAttribute(Common)]
        [RefreshProperties(RefreshProperties.All)]
        [NotifyParentProperty(true)]       
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [Required]
        public string ComPortCommon
        {
            get { return _comPortCommon; }
            set
            {
                if (_comPortCommon == value)
                    return;            
                _comPortCommon = value;
                ini.IniWriteValue("COM", "Name", _comPortCommon);
                OnPropertyChanged();
            }
        }

        [PropertyOrder(1)]
        [CategoryAttribute(Common)]
        [Description("Длительность")]
        [DisplayName("Длительность")]        
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(0, 1000000)]
        public int Duration
        {
            get { return _duration; }
            set
            {
                if (_duration == value)
                    return;
                if (_duration < 0)
                    return;
                 _duration = value;
                OnPropertyChanged();
                ini.IniWriteValue("Parameters", "duration", Convert.ToString(_duration));                
            }
        }

        [PropertyOrder(0)]
        [CategoryAttribute(Common)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        public string Language
        {
            get { return _language; }
            set
            {
                if (_language == value)
                    return;
                _language = value;
                OnPropertyChanged();
            }
        }


        [PropertyOrder(4)]
        [CategoryAttribute(Common)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        public Version VersionType
        {
            get { return _versionType; }
            set
            {
                if (_versionType == value)
                    return;
                _versionType = value;
                ini.IniWriteValue("Parameters", "versionType", Convert.ToString(_versionType));
                OnPropertyChanged();
            }
        }

        #endregion

        #region FWS
        [PropertyOrder(0)]
        [Category(FWS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [Range(1500, 6000000)]
        public double FrequencyFWS
        {
            get { return _frequencyFWS; }
            set
            {
                if (_frequencyFWS == value)
                    return;
                if (_frequencyFWS < 0)
                    return;
                _frequencyFWS = value;
                ini.IniWriteValue("Parameters", "FrequencyFWS", Convert.ToString(_frequencyFWS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(2)]
        [CategoryAttribute(FWS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(100, 5700000)]
        public double StepFWS
        {
            get { return _stepFWS; }
            set { if (_stepFWS == value)
                    return;
                if (_stepFWS < 0)
                    return;
                _stepFWS = value;
                ini.IniWriteValue("Parameters", "stepFWS", Convert.ToString(_stepFWS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(3)]
        [CategoryAttribute(FWS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(0, 1000000)]
        public int TimeFWS
        {
            get { return _timeFWS; }
            set
            {
                if (_timeFWS == value)
                    return;
                if (_timeFWS < 0)
                    return;
                _timeFWS = value;
                ini.IniWriteValue("Parameters", "timeFWS", Convert.ToString(_timeFWS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(4)]
        [CategoryAttribute(FWS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        public bool AutoFWS
        {
            get { return _autoFWS; }
            set {
                if (_autoFWS == value)
                    return;
                _autoFWS = value;
                ini.IniWriteValue("Parameters", "auto", Convert.ToString(_autoFWS));
                OnPropertyChanged(); }
        }

        [PropertyOrder(1)]
        [CategoryAttribute(FWS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(1500, 6000000)]
        public double EndFreqencyFWS
        {
            get { return _endFreqencyFWS; }
            set
            {
                if (_endFreqencyFWS == value)
                    return;
                if (_endFreqencyFWS < 0)
                    return;
                _endFreqencyFWS = value;
                ini.IniWriteValue("Parameters", "endFreqencyFWS", Convert.ToString(_endFreqencyFWS));
                OnPropertyChanged();
            }
        }
        #endregion

        #region FUSS
        [PropertyOrder(0)]
        [CategoryAttribute(FHSS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(1500, 6000000)]
        public double FrequencyFUSS
        {
            get { return _frequencyFUSS; }
            set {
                if (_frequencyFUSS == value)
                    return;
                if (_frequencyFUSS < 0)
                    return;
                _frequencyFUSS = value;
                ini.IniWriteValue("Parameters", "frequencyFUSS", Convert.ToString(_frequencyFUSS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(1)]
        [CategoryAttribute(FHSS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        public int AmountOfIterationFUSS
        {
            get { return _amountOfIterationFUSS; }
            set {
                if (_amountOfIterationFUSS == value)
                    return;
                if (_amountOfIterationFUSS < 0)
                    return;
                _amountOfIterationFUSS = value;
                ini.IniWriteValue("Parameters", "amountOfIterationFUSS", Convert.ToString(_amountOfIterationFUSS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(3)]
        [CategoryAttribute(FHSS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(1, 10000)]
        public int AmountOfSignalPerSecFUSS
        {
            get { return _amountOfSignalPerSecFUSS; }
            set
            {
                if (_amountOfSignalPerSecFUSS == value)
                    return;
                if (_amountOfSignalPerSecFUSS < 0)
                    return;
                _amountOfSignalPerSecFUSS = value;
                ini.IniWriteValue("Parameters", "amountOfSignalPerSecFUSS", Convert.ToString(_amountOfSignalPerSecFUSS));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(2)]
        [CategoryAttribute(FHSS)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        [Range(100, 5700000)]
        public int StepOfFreqencyFUSS
        {
            get { return _stepOfFreqencyFUSS; }
            set
            {
                if (_stepOfFreqencyFUSS == value)
                    return;
                if (_stepOfFreqencyFUSS < 0)
                    return;
                _stepOfFreqencyFUSS = value;
                ini.IniWriteValue("Parameters", "stepOfFreqencyFUSS", Convert.ToString(_stepOfFreqencyFUSS));
                OnPropertyChanged();
            }
        }
        #endregion

        #region Grid
        [PropertyOrder(0)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [CategoryAttribute(Grid)]
        [Required]
        public string RangeFW
        {
            get { return _rangeFW; }
            set
            {
                if (_rangeFW == value)
                      return;
                 _rangeFW = value;
                OnPropertyChanged();
            }
        }

        [PropertyOrder(1)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [CategoryAttribute(Grid)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        public int StepFW
        {
            get { return _stepFW; }
            set
            {
                if (_stepFW == value)
                    return;
                if (_stepFW < 0)
                    return;
                _stepFW = value;
                ini.IniWriteValue("Parameters", "stepFW", Convert.ToString(_stepFW));
                OnPropertyChanged();
            }
        }

        [PropertyOrder(2)]
        [Description("Длительность")]
        [DisplayName("Длительность")]
        [CategoryAttribute(Grid)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Required]
        public bool Offset
        {
            get { return _offset; }
            set
            {
                 if (_offset == value)
                   return;
                _offset = value;                
                ini.IniWriteValue("Parameters", "offset", Convert.ToString(_offset));
                if (_offset == true)
                    NumericUpDownx2.OffsetKoef = 0.5;
                else NumericUpDownx2.OffsetKoef = 0;
                OnPropertyChanged();              
            }
        }
      
        #endregion

        #region IMethod

        public PG Clone()
        {
            return new PG()
            {              
                //LenguageCommon = lenguageCommon,
                SpeedCommon = _speedCommon,
                ComPortCommon = _comPortCommon,
                TimeFWS = _timeFWS,
                AutoFWS = _autoFWS,
                StepFWS = _stepFWS,
                FrequencyFWS = _frequencyFWS,
                FrequencyFUSS = _frequencyFUSS,
                StepOfFreqencyFUSS = _stepOfFreqencyFUSS,
                AmountOfSignalPerSecFUSS = _amountOfSignalPerSecFUSS,
                AmountOfIterationFUSS = _amountOfIterationFUSS,
                RangeFW = _rangeFW,            
                StepFW = _stepFW,
                Offset = _offset,
                Duration = _duration,
                VersionType = _versionType,
            };
        }

        public void Update(PG data)
        {
            //LenguageCommon = data.LenguageCommon;
            SpeedCommon = data.SpeedCommon;
            ComPortCommon = data.ComPortCommon;
            FrequencyFWS = data.FrequencyFWS;
            TimeFWS = data.TimeFWS;
            AutoFWS = data.AutoFWS;
            StepFWS = data.StepFWS;          
            FrequencyFWS = data.FrequencyFWS;
            FrequencyFUSS = data.FrequencyFUSS;
            AmountOfIterationFUSS = data.AmountOfIterationFUSS;
            StepOfFreqencyFUSS = data.StepOfFreqencyFUSS;
            AmountOfSignalPerSecFUSS = data.AmountOfSignalPerSecFUSS;
            RangeFW = data.RangeFW;
            StepFW = data.StepFW;
            Offset = data.Offset;
            Duration = data.Duration;
            VersionType = data.VersionType;
        }

        public bool EqualTo(PG data)
        {
            return /*lenguageCommon != data.LenguageCommon || */ _speedCommon != data.SpeedCommon || _comPortCommon != data.ComPortCommon ||
                _frequencyFWS != data.FrequencyFWS || _stepFWS != data.StepFWS || _autoFWS != data.AutoFWS || _timeFWS != data.TimeFWS ||
                _frequencyFUSS != data.FrequencyFUSS || _amountOfIterationFUSS != data.AmountOfIterationFUSS || _stepOfFreqencyFUSS != data.StepOfFreqencyFUSS || _amountOfSignalPerSecFUSS != data.AmountOfSignalPerSecFUSS ||
                 _rangeFW != data.RangeFW || _stepFW != data.StepFW || _offset != data.Offset || _duration != data.Duration || _versionType != data.VersionType
                ? false
                : true;
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}

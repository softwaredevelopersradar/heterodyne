﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heterodyne.PropGridClasses
{
   class DictionaryForLocalizationEN
   {
       public  Dictionary<string, string> ENPropDict = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "Frequency, kHz"},
            { nameof(PG.TimeFWS), "Time, sec."},
            { nameof(PG.AutoFWS), "Auto"},
            { nameof(PG.StepFWS), "Tuning step,\nMHz" },

            { nameof(PG.AmountOfSignalPerSecFUSS), "Number of\njumps, j/sec."},
            { nameof(PG.StepOfFreqencyFUSS), "Tuning step,\nkHz"},
            { nameof(PG.FrequencyFUSS), "Frequency, kHz"},
            { nameof(PG.AmountOfIterationFUSS), "Number\nfrequency" },

            { nameof(PG.ComPortCommon), "COM Port" },
            { nameof(PG.Language), "Language" },
            { nameof(PG.SpeedCommon), "Speed" },

            { nameof(PG.StepFW), "Tuning step,\nMHz" },
            { nameof(PG.RangeFW), "Range, MHz\n\nLine" },
            { nameof(PG.Offset), "Offset" },
            { nameof(PG.Duration), "Duration, sec." },
            { nameof(PG.EndFreqencyFWS), "Final frequency,\nkHz" },
            { nameof(PG.VersionType), "Version" },
        };

       public   Dictionary<string, string> RUPropDict = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "Частота, кГц"},
            { nameof(PG.TimeFWS), "Время, с."},
            { nameof(PG.AutoFWS), "Авто"},
            { nameof(PG.StepFWS), "Шаг престройки,\nкГц" },

            { nameof(PG.AmountOfSignalPerSecFUSS), "Кол-во\nскачков, ск/с"},
            { nameof(PG.StepOfFreqencyFUSS), "Шаг перестройки,\nкГц"},
            { nameof(PG.FrequencyFUSS), "Частота, кГц"},
            { nameof(PG.AmountOfIterationFUSS), "Колличество\nчастот" },

            { nameof(PG.ComPortCommon), "COM Порт" },
            { nameof(PG.Language), "Язык" },
            { nameof(PG.SpeedCommon), "Скорость" },

            { nameof(PG.StepFW), "Шаг престройки,\nМГц" },
            { nameof(PG.RangeFW), "Диапозон, МГц\n\nПолоса" },
            { nameof(PG.Offset), "Смещение" },
            { nameof(PG.Duration), "Длительность,\nсек." },
            { nameof(PG.EndFreqencyFWS), "Конечная частота,\nкГц" },
            { nameof(PG.VersionType), "Версия" },
        };

       public Dictionary<string, string> ENDiscrDict = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "Start frequency"},
            { nameof(PG.TimeFWS), "Signal sending time"},
            { nameof(PG.AutoFWS), "Automatical overhaul"},
            { nameof(PG.StepFWS), "Tuning step" },

            { nameof(PG.AmountOfSignalPerSecFUSS), "Jumps per second"},
            { nameof(PG.StepOfFreqencyFUSS), "Tuning step"},
            { nameof(PG.FrequencyFUSS), "Start frequency"},
            { nameof(PG.AmountOfIterationFUSS), "Number of frequencies for tuning" },

            { nameof(PG.ComPortCommon), "Name of COM port" },
            { nameof(PG.Language), "Interface language" },
            { nameof(PG.SpeedCommon), "Speed of COM port" },

            { nameof(PG.StepFW), "Interval between frequency" },
            { nameof(PG.RangeFW), "Frequency range" },
            { nameof(PG.Offset), "Frequency offset" },
            { nameof(PG.Duration), "Radiation duration" },
            { nameof(PG.EndFreqencyFWS), "Final frequency" },
            { nameof(PG.VersionType), "Version" },
        };

       public Dictionary<string, string> RUDiscrDict = new Dictionary<string, string>()
        {
            { nameof(PG.FrequencyFWS), "Начальная частота"},
            { nameof(PG.TimeFWS), "Время посылания сигнала"},
            { nameof(PG.AutoFWS), "Автоматическая перестройка"},
            { nameof(PG.StepFWS), "Шаг престройки" },

            { nameof(PG.AmountOfSignalPerSecFUSS), "Колличество скачков в секунду"},
            { nameof(PG.StepOfFreqencyFUSS), "Шаг перестройки"},
            { nameof(PG.FrequencyFUSS), "Начальная частота"},
            { nameof(PG.AmountOfIterationFUSS), "Количество частот для перестройки" },

            { nameof(PG.ComPortCommon), "Имя COM порта" },
            { nameof(PG.Language), "Язык интерфейса" },
            { nameof(PG.SpeedCommon), "Скорость COM порта" },

            { nameof(PG.StepFW), "Интервал между частотами" },
            { nameof(PG.RangeFW), "Диапозон частот" },
            { nameof(PG.Offset), "Смещение частоты" },
            { nameof(PG.Duration), "Диапозон частот" },
            { nameof(PG.EndFreqencyFWS), "Конечная частота" },
            { nameof(PG.VersionType), "Версия" },
        };
          
        public Dictionary<string, string> RUCateg = new Dictionary<string, string>()
        {
            { "FWS", "ФРЧ"},          
            { "FHSS", "ППРЧ"},           
            { "Common", "Общее" },           
            { "Grid", "Сетка частот" },           
        };
        public Dictionary<string, string> ENCateg = new Dictionary<string, string>()
        {
            { "FWS", "FWS"},
            { "FHSS", "FHSS"},
            { "Common" , "Common"},
            {"Grid" , "Grid"},
        };
    }
}

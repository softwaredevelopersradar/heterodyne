﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System;

namespace Heterodyne.Localization
{
        [XmlType]
  public class Language1
  {
       public Language1() { }
        #region RadioButton
        [XmlElement]
            public string RB1
            { get; set; }

            [XmlElement]
            public string RB2
            { get; set; }

            [XmlElement]
            public string RB3
            { get; set; }
        #endregion

        #region Button
        [XmlElement]
        public string BtnClear
        { get; set; }

        [XmlElement]
        public string BtnOff
        { get; set; }
        #endregion

        #region Data Button
        [XmlElement]
        public string LTemp
        { get; set; }
        [XmlElement]
        public string LHumid
        { get; set; }
        [XmlElement]
        public string LCharge
        { get; set; }
        [XmlElement]
        public string LError
        { get; set; }
        [XmlElement]
        public string LPower
        { get; set; }
        #endregion

        #region Error 
        [XmlElement]
        public string Error1
        { get; set; }
        [XmlElement]
        public string Error2
        { get; set; }
        [XmlElement]
        public string Error3
        { get; set; }
        [XmlElement]
        public string Error4
        { get; set; }
        [XmlElement]
        public string Error5
        { get; set; }
        [XmlElement]
        public string Error6
        { get; set; }
        [XmlElement]
        public string Error7
        { get; set; }
        [XmlElement]
        public string Error8
        { get; set; }
        [XmlElement]
        public string Error9
        { get; set; }
        #endregion

        #region Text
        [XmlElement]
        public string Text1
        { get; set; }
        [XmlElement]
        public string Text2
        { get; set; }
        [XmlElement]
        public string Text3
        { get; set; }
        public string Text4
        { get; set; }
        #endregion

        #region StatusBar
        [XmlElement]
        public string StatusBar1
        { get; set; }
        [XmlElement]
        public string StatusBar2
        { get; set; }

        [XmlElement]
        public string StatusBar3
        { get; set; }

        [XmlElement]
        public string StatusBar4
        { get; set; }

        [XmlElement]
        public string StatusBar5
        { get; set; }

        #endregion

        #region Log
        [XmlIgnore]
        public string Log1
        { get; set; }
        [XmlIgnore]
        public string Log2
        { get; set; }
        #endregion

        #region LErrorTB
        [XmlIgnore]
        public string LErrorTB
        { get; set; }

        [XmlIgnore]
        public string LErrorTB1
        { get; set; }

        [XmlIgnore]
        public string ErrorH
        { get; set; }
        #endregion

        #region InfoGrid
        [XmlElement]
        public string Step
        { get; set; }
        [XmlElement]
        public string Time
        { get; set; }
        [XmlElement]
        public string Time1
        { get; set; }
        #endregion

        #region HetrError
        [XmlElement]
        public string HetrError1
        { get; set; }

        [XmlElement]
        public string HetrError2
        { get; set; }


        [XmlElement]
        public string HetrError3
        { get; set; }

        [XmlElement]
        public string HetrError4
        { get; set; }

        [XmlElement]
        public string HetrError5
        { get; set; }

        [XmlElement]
        public string HetrErrorD
        { get; set; }

        [XmlElement]
        public string HetrError10
        { get; set; }

        [XmlElement]
        public string HetrError11
        { get; set; }

        #endregion

        #region switch
        [XmlElement]
        public string Switch1
        { get; set; }

        [XmlElement]
        public string Switch2
        { get; set; }

        [XmlElement]
        public string Switch3
        { get; set; }
        #endregion

        [XmlIgnore]
        public string MessError1
        { get; set; }

        [XmlIgnore]
        public string Name
        { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public static List<Language1> Load()
        {
            var xmlSerializer = new XmlSerializer(typeof(Language1));
            string pathToLocales = System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + @"\Localization\Language";
            var result = new List<Language1>();

            foreach (var file in new DirectoryInfo(pathToLocales).GetFiles("*.xml"))
            {
                using (var reader = new XmlTextReader(file.FullName))
                {
                    var language = (Language1)xmlSerializer.Deserialize(reader);
                    language.Name = Path.GetFileNameWithoutExtension(file.Name);
                    result.Add(language);
                }
            }
            
            return result;
        }

        private static string SubStrDel(string str, string substr)
        {
            int n = str.IndexOf(substr);
            str = str.Remove(n, substr.Length);
            return str;
        }
    }
}
